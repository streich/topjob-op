'use strict';

exports.render = function(req, res) {
    res.data.pageScript = 'static';
    res.render('static.html');
};
