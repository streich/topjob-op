'use strict';

exports.render = function(req, res) {
    res.data.pageScript = 'index';
    res.render('index.html');
};
