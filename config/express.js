'use strict';

var morgan = require('morgan');

module.exports = function(app) {

    app.set('showStackError', true);

    app.use(morgan('dev'));

    // Setup view engine
    require('./views')(app);

    // Continue to routing,
    require('./routing')(app);
};
