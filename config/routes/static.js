'use strict';
/**
 * Static
 * Routes that manage static methods
 */

var express = require('express'),
    compression = require('compression'),
    config = require(root + '/config');

module.exports = function(app) {
    var staticLocation = config.staticLocation || 'client';
    var assetsLocation = config.assetsLocation || 'client';

    app.use(compression());

    // Static assets should be cached 5ever
    var oneDay      = 86400000;
    var oneWeek     = oneDay * 7;
    // var oneYear     = oneWeek * 52;
    var maxAge      = (config.maxAge !== undefined) ?
        config.maxAge :
        oneWeek ;

    //
    // TODO:
    // Have to implement cache-busting for static assets, as well as a
    // method for re-writing the cache-busted assets
    //
    // /assets/assetName.[md5].ext => /assets/assetName.ext
    //

    // Static client scripts
    app.use('/assets', express.static(config.root + '/' + assetsLocation, { maxAge: maxAge }));
    app.use('/client', express.static(config.root + '/' + assetsLocation, { maxAge: maxAge }));
    app.use(express.static(config.root + '/' + staticLocation, { maxAge: maxAge }));
};
