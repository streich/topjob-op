var path = require('path'),
    root = path.normalize(__dirname + '/../..');

// I AM ROOT
GLOBAL.root = root;

module.exports = {
    defaultController: 'index',
    defaultControllerMethod: 'render',
    secret: 'secretdefaultsecret',
    root: root,
    port: process.env.PORT || 3000,
    domain: process.env.DOMAIN || 'localhost',
    db: {},
    auth: {}
};
