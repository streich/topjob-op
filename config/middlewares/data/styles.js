/**
 * Styles
 * Adds variables and data for stylesheet performance,
 * mainly inline styles for the page
 */

var fs = require('fs');

module.exports = function(req, res, next) {
    'use strict';

    fs.readFile(root + '/dist/css/main.css', 'utf8', function(err, data) {
    // fs.readFile(root + '/dist/css/inline.css', 'utf8', function(err, data) {
        if (data) res.data.inlineStyles = data;
        next();
    });
};
