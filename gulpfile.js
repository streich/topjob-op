'use strict';
/* jshint unused:false */

var gulp = require('gulp'),
    bower = require('gulp-bower'),
    browserify = require('gulp-browserify'),
    compass = require('gulp-compass'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-csso'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    svgmin = require('gulp-svgmin'),
    rimraf = require('gulp-rimraf'),
    nodemon = require('gulp-nodemon'),
    download = require('gulp-download'),
    replace = require('gulp-replace'),
    rename = require('gulp-rename'),
    mocha = require('gulp-mocha');


// Configuration Directories
var dir = {
    app:    'app',
    client: 'client',
    config: 'config',
    dist:   'dist'
};

function handleError(err) {
    /* jshint validthis:true */
    console.log(err.toString());
    this.emit('end');
}

gulp.task('rimraf', function() {
    return gulp.src(dir.dist, {read: false})
        .pipe(rimraf());
});

gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(dir.client + '/scripts/vendor'));
});

gulp.task('bower-styles', function() {
    return bower({
        cwd: dir.client + '/sass'
    });
});

gulp.task('styles-sass', ['bower-styles'], function() {

// Sass task
     // sass(dir.client + '/sass/main.scss')
     //    .on('error', function (err) { console.log(err.message); })
     //    .pipe(autoprefixer('last 1 version', '> 1%', 'ie 8'))
     //    .pipe(gulp.dest(dir.client + '/css'));

    // return gulp.src(dir.client + '/sass/*.scss')
    //     .pipe(sass().on('error', handleError))
    //     .pipe(gulp.dest(dir.client + '/css'));  

  // gulp.src('./sass/**/*.scss')
  //   .pipe(sass().on('error', sass.logError))
  //   .pipe(gulp.dest('./css'));


    return gulp.src(dir.client + '/sass/*.{scss,sass}')
        .pipe(compass({
            style: 'expanded',
            css: dir.client + '/css',
            sass: dir.client + '/sass',
            require: []
        }))
        .on('error', handleError)
        .pipe(autoprefixer())
        .pipe(gulp.dest(dir.client + '/css'));
});

gulp.task('styles', ['styles-sass'], function() {
    return gulp.src(dir.client + '/css/**')
        .pipe(minifycss())
        .pipe(gulp.dest(dir.dist + '/css'));
});

gulp.task('scripts-browserify', function() {
    return gulp.src([
            dir.client + '/scripts/**/*.js',
            '!' + dir.client + '/scripts/models/**',
            '!' + dir.client + '/scripts/view-models/**',
            '!' + dir.client + '/scripts/lib/**',
            '!' + dir.client + '/scripts/components/**',
            '!' + dir.client + '/scripts/vendor/**',
            '!' + dir.client + '/scripts/templates/**',
            '!' + dir.client + '/scripts/modules/**',
        ])
        .on('error', handleError)
        .pipe(browserify())
        .on('error', handleError)
        .pipe(gulp.dest(dir.client + '/js'));
});

gulp.task('clientScripts', ['clientLint', 'scripts-browserify'], function() {
    return gulp.src(dir.client + '/js/**/*.js')
        // .pipe(uglify())
        .pipe(gulp.dest(dir.dist + '/js'));
});

gulp.task('images', function() {
    return gulp.src(dir.client + '/images/**/*.{png,jpg,jpeg}')
        .pipe(
            imagemin({
                optimizationLevel: 3,
                progressive: true,
                interlaced: true
            }))
        .pipe(gulp.dest(dir.dist + '/images/'));
});

gulp.task('gif', function() {
    return gulp.src(dir.client + '/images/**/*.gif')
        .pipe(gulp.dest(dir.dist + '/images/'));
});

gulp.task('svg', function() {
    return gulp.src(dir.client + '/images/**/*.svg')
        .pipe(
            svgmin()
        )
        .pipe(gulp.dest(dir.dist + '/images/'));
});

gulp.task('fonts', function() {
    return gulp.src(dir.client + '/fonts/**')
        .pipe(gulp.dest(dir.dist + '/fonts/'));
});

gulp.task('watch', ['client'], function() {

    // Watch client scripts
    gulp.watch(dir.client + '/scripts/**/*.js', ['clientScripts']);

    // Watch image files
    gulp.watch(dir.client + '/images/**/*.{png,jpg,jpeg}', ['images']);

    // Watch svg files
    gulp.watch(dir.client + '/images/**/*.svg', ['svg']);

    // Watch font files
    gulp.watch(dir.client + '/fonts/**', ['fonts']);

    // Watch styles
    gulp.watch([
            dir.client + '/sass/**/*.{sass,scss}',
        ], ['styles']);
});

gulp.task('mocha', function() {
    gulp.src('./test/**/*.js')
        .pipe(mocha({ reporter: 'list' }));
});

gulp.task('lint', function() {
    return gulp.src([
            'gulpfile.js',
            'app/**/*.js',
            'config/**/*.js',
            'lib/**/*.js',
            'test/**/*.js'
        ])
        // .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('default'));
});

gulp.task('clientLint', function() {
    return gulp.src([
            dir.client + '/scripts/**/*.js',
            '!' + dir.client + '/scripts/vendor/**',
        ])
        // .pipe(jshint('.jshintrc'))
        .pipe(jshint.reporter('default'));
});

gulp.task('client', ['rimraf', 'bower'], function() {
    gulp.start('styles', 'clientScripts', 'images', 'gif', 'svg', 'fonts');
});

gulp.task('test', ['lint', 'mocha']);

gulp.task('app', function() {
    return nodemon({
            script: 'server.js',
            ignore: [
                'README.md',
                'node_modules/**',
                dir.client,
                dir.dist
            ],
            watchedExtensions: ['js', 'json', 'html'],
            watchedFolders: [dir.app, 'config', 'views'],
            debug: true,
            delayTime: 1,
            env: {
                NODE_ENV: 'local',
                PORT: 4000,
            },
        })
        .on('restart', ['test']);
});

gulp.task('build-static', function() {
    download('http://localhost:4000')
        .pipe(replace(/\/assets/gi, 'client'))
        .pipe(replace(/"assets/gi, '"client'))
        .pipe(replace(/\.\.\/images/gi, 'client/images'))
        .pipe(rename('index.html'))
        .pipe(gulp.dest('./'))
        .pipe(rename('index.php'))
        .pipe(gulp.dest('./'));

    download('http://localhost:4000/static')
        .pipe(replace(/\/assets/gi, 'client'))
        .pipe(replace(/"assets/gi, '"client'))
        .pipe(replace(/\.\.\/images/gi, 'client/images'))
        .pipe(rename('static.html'))
        .pipe(gulp.dest('./'))
        .pipe(rename('static.php'))
        .pipe(gulp.dest('./'));
});

// Restart the app server
gulp.task('app-restart', function() {
    nodemon.emit('restart');
});

/** Build it all up and serve it */
gulp.task('default', ['app', 'watch']);

// /** Build it all up and serve the production version */
// gulp.task('serve', ['app', 'client', 'watch']);
