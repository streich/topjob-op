(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
/* global console */

function Button(opts) {
	var self = this,
		font = opts.fs + "px " + opts.font,
		ctx = opts.ctx,
		color = opts.color;

	self.rect = {
		x: opts.x,
		y: opts.y,
		w: opts.w || getBtnWidth(opts.wFactor),
		h: opts.h || Math.ceil(opts.fs * opts.hFactor)
	};
	self.textX = self.rect.x + self.rect.w/2;
	self.textY = self.rect.y + self.rect.h/2;

	self.draw = function () {
		ctx.save();
            ctx.textAlign = "center";
            ctx.textBaseline = "middle";
            ctx.fillStyle = color;
            ctx.fillRect(self.rect.x, self.rect.y, self.rect.w, self.rect.h);
            ctx.fillStyle = opts.textColor;
            ctx.font = font;
            ctx.fillText(opts.text, self.textX, self.textY);
        ctx.restore();
	};

	self.checkButton = function (mouse) {
		var test = pointInRect (mouse, self.rect);
		color = ( test ) ? opts.activeColor : opts.color;
		return ( test ) ? self : false;
	};

	self.getText = function () {
		return opts.text;
	};

	function getBtnWidth(k) {
		var w;
		ctx.save();
		    ctx.font = font;
		    w = ctx.measureText(opts.text).width;
		ctx.restore();
		return Math.ceil(w * k);
	}
}
},{}]},{},[1])