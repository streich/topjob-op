(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
/* jslint ignore:start */

module.exports = (function() {

    function getGeometry() {
        var obj = {};
        // obj.w = window.innerWidth || document.body.offsetWidth;
        // obj.h = window.innerHeight || document.body.offsetHeight;
        obj.w =  document.body.offsetWidth;
        obj.h =  document.body.offsetHeight;
        obj.cx = obj.w/2;
        obj.cy = obj.h/2;
        return obj;
    }

    function filterInt(value) {
        if(/^(\-|\+)?([0-9]+|Infinity)$/.test(value))
            return Number(value);

        return NaN;
    }


    function getModalMarginSize(value) {
        var marginPercent = 18;

        if(value) {
            var intModalSize = this.filterInt(value);

            if(!isNaN(intModalSize) && intModalSize > 0 && intModalSize !== Infinity)
                marginPercent = ((100 - value) / 2);
        }

        return { marginLeft: marginPercent + '%', marginRight: marginPercent + '%' };
    }

    return {
        getGeometry: getGeometry,
        filterInt: filterInt,
        getModalMarginSize: getModalMarginSize
    };

}) ();


},{}]},{},[1])