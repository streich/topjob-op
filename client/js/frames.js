(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
/* jslint ignore:start */

function Frames(opts) {
	var self = this,
		ctx = opts.ctx,
		frameX, frameY, frames,
		frLen, lastFrame, perStage,
		maxFrame,
		step = -1,
		_step = -1,
		stages = {
			'CAGE CLAMP TECHNOLOGY': { step: 0, index: 0 },
			'UNIVERSELLER LEITERANSCHLUSS': { step: 0, index: 1 },
			'HOHE SICHERHEITSRESERVEN': { step: 0, index: 2 },
			'MULTIFUNKTIONALES BRÜCKERSYSTEM': { step: 0, index: 3 },
			'KOMFORTABLE BESCHRIFTUNG': { step: 0, index: 4 },
			'ALLE PRODUKTVORTEILE': { step: 0, index: 5 },
		},
		sloganFrames = opts.framesForSlogan,
		activeStage;

	self.init = function(images) {
		var item;
		frames = images;
		frLen = frames.length;
		lastFrame = frLen - 1;
		maxFrame = lastFrame + 67;

		// ----------------------
		// below is workaround to distribute remaining frames
		perStage = Math.round((maxFrame - 115)/5);
		for ( item in stages ) {
			if ( stages.hasOwnProperty(item) ) {
				stages[item].step = 115 + perStage * stages[item].index;
//				stages[item].panel = new CheckPointPanel({
//					ctx: opts.ctx,
//					start: stages[item].step - sloganFrames,
//					frames: sloganFrames,
//					still: opts.stillState,
//					text: item,
//					g: opts.g,
//					onShowDetails: function () {
//						opts.allowScroll();
//					}
//				});
			}
		}
		stages['ALLE PRODUKTVORTEILE'].step = maxFrame;
		// ----------------------

		frameX = opts.g.w; // all frames should be of equal size!
		frameY = opts.g.h;
	};

	self.step = function(n) {
		_step = step;
		step += n;
		if ( step < -1 ) {
			step = _step;
		}else if ( step > maxFrame ) {
			step = _step;
		}else {
			checkStage();
			n = ( step < 0 ) ? 0 : step;
			if ( activeStage ) { stages[activeStage].panel.update(n); }
			nextFrame(n);
		}
	};

	self.setStage = function(name) { frameAnimation(stages[name].step); };

	self.nextStage = function() {
		var item;
		if ( activeStage ) {
			if ( activeStage === 'ALLE PRODUKTVORTEILE' ) { return; }
			for ( item in stages ) {
				if ( stages.hasOwnProperty(item) && item !== activeStage ) {
					if ( stages[item].index === stages[activeStage].index + 1 ) {
						frameAnimation(stages[item].step);
						break;
					}
				}
			}
		}else {
			frameAnimation(stages['CAGE CLAMP TECHNOLOGY'].step);
		}
	};

	self.resetMenu = function() { setMenu(); };

	function nextFrame(n) {
		var index = ( n > lastFrame ) ? lastFrame : n; // to not load last apr. 65 frames which is identical
		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
		ctx.drawImage(frames[index], frameX, frameY);
		if ( activeStage ) { stages[activeStage].panel.draw(); }
	}

	function checkStage() {
		var item;
		for ( item in stages ) {
			if ( step > _step ) {
//				if ( stages[item].panel.start === step ) {
//					activeStage = item;
//					setMenu(item);
//					break;
//				}else if ( stages[item].step - 3 === step ) {
//					stages[item].panel.listen();
//					break;
//				}else if ( stages[item].step === step ) {
//					opts.disallowScroll();
//					break;
//				}else if ( stages[item].step + 1 === step ) {
//					stages[item].panel.reset();
//					activeStage = null;
//					setMenu();
//					break;
//				}
			}else if ( step < _step ) {
//				if ( stages[item].step === step ) {
//					activeStage = item;
//					setMenu(item);
//					stages[item].panel.listen();
//					break;
//				}else if ( stages[item].panel.start === _step ) {
//					stages[item].panel.reset();
//					activeStage = null;
//					setMenu();
//					break;
//				}
			}
		}
	}

	function frameAnimation(target) {
		opts.disallowScroll();
		var delta = ( step < target ) ? 1 : -1,
			rAF = requestAnimationFrame(loop);
		function loop() {
			self.step(delta);
			if ( step !== target ) {
				rAF = requestAnimationFrame(loop);
			}else {
				cancelAnimationFrame(rAF);
			}
		}

	}

}

	// self.next = function(n) {
	// 	ctx.drawImage(frames[n], 0, 0, imgWidth, imgHeight, shiftX, shiftY, imgW, imgH);
	// };

	// self.setImages = function(images) {
	// 	var img, ratio;
	// 	frames = images;
	// 	img = frames[0];
	// 	imgWidth = img.width;
	// 	imgHeight = img.height;
	// 	ratio = Math.max(w/imgWidth, h/imgHeight);
	// 	shiftX = (w  - imgWidth * ratio)/2;
	// 	shiftY = (h - imgHeight * ratio)/2;
	// 	imgW = imgWidth * ratio;
	// 	imgH = imgHeight * ratio;
	// };

},{}]},{},[1])