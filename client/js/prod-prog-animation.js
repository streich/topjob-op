(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
/* jslint ignore:start */

var rhomb = {
    set: function (x, y, s, c) {
        var delta = Math.sqrt(2) * s;
        return {
            color: c,
            north: { x: x, y: y },
            east: { x: x + delta, y: y + delta },
            south: { x: x, y: y + delta * 2 },
            west: { x: x - delta, y: y + delta }
        };
    },
    update: function (dy, r) {
        r.north.y += dy;
        r.east.y += dy;
        r.south.y += dy;
        r.west.y += dy;
    },
    draw: function (ctx, r) {
        ctx.save();
            ctx.fillStyle = r.color;
            ctx.beginPath();
            ctx.moveTo(r.north.x, r.north.y);
            ctx.lineTo(r.east.x, r.east.y);
            ctx.lineTo(r.south.x, r.south.y);
            ctx.lineTo(r.west.x, r.west.y);
            ctx.closePath();
            ctx.fill();
        ctx.restore();
    },
    getDiagonal: function (s) {
        return Math.sqrt(2) * s * 2;
    }
};

function PPAnimation(ctx, g) {
    var self = this,
        deviceImage = new Image(),
        panels, len,
        rAF, activeButton;

    deviceImage.onload = function () {
        // console.log('images/misc/WAGO_device.png');
    };
    deviceImage.src = 'client/images/misc/WAGO_device.png';

    self.init = function() {

        clear();
        removeEventListeners();
        if ( rAF ) { cancelAnimation(); }
        panels = [];
        activeButton = false;

        var rs = Math.round(g.w/12),
            dgnl = rhomb.getDiagonal(rs),
            gap = Math.round(dgnl/20),
            finY = Math.round(g.h/6),
            dist = g.h * 0.85,
            startY = finY - dist;

        panels[0] = new Panel({
            ctx: ctx,
            x: g.cx - (dgnl + gap)/2,
            y: startY + dgnl/2 + gap,
            finY: finY + dgnl/2 + gap,
            color: '#E5E5E5',
            side: rs,
            text: 'Funktionslemmen'
        });

        panels[1] = new Panel({
            ctx: ctx,
            x: g.cx + (dgnl + gap)/2,
            y: startY + dgnl/2 + gap,
            finY: finY + dgnl/2 + gap,
            color: '#E5E5E5',
            side: rs,
            text: 'Durchgangsklemmen'
        });

        panels[2] = new Panel({
            ctx: ctx,
            x: g.cx + dgnl + gap,
            y: startY,
            finY: finY,
            color: '#E5E5E5',
            side: rs,
            text: 'Installationsetagenklemmen'
        });

        panels[3] = new Panel({
            ctx: ctx,
            x: g.cx - dgnl - gap,
            y: startY,
            finY: finY,
            color: '#E5E5E5',
            side: rs,
            text: 'Verteilereinspeiseklemmen'
        });

        panels[4] = new MainPanel({
            ctx: ctx,
            x: g.cx,
            y: startY,
            finY: finY,
            color: '#57AA53',
            side: rs
        });

        len = panels.length;

        for ( var i = 0; i < len; i++ ) {
            if ( i < len - 1 ) { panels[i].setImage(deviceImage); }
        }
    };

    function Panel(opts) {
        var self = this,
            ctx = opts.ctx,
            fs = Math.round(opts.side/6),
            buttonFs = Math.round(opts.side/7),
            rquote = String.fromCharCode(187),
            dImage = {},
            rb = rhomb.set(opts.x, opts.y, opts.side, opts.color),
            half = opts.y + (opts.finY -  opts.y)/8,
            text = {
                text: opts.text,
                font: fs + 'px futura',
                color: '#333',
                x: opts.x,
                y: rb.west.y + fs * 1.5
            },
            button = {
                y: rb.west.y + (rb.south.y - rb.west.y)/3,
                w: (rb.west.x - rb.east.x)/2.8,
                h: fs * 1.7,
                color: '#57AA53',
                text: rquote + ' zum E-Shop',
                font: buttonFs + 'px futura',
                textColor: '#FFF',
                textX: opts.x
            };

        // var pCanvas, pctx, px, py;

        button.textY = button.y + button.h/2;
        button.x = opts.x - button.w/2;

        self.half = false;

        self.draw = function() {
            rhomb.draw(ctx, rb);
            ctx.drawImage(dImage.image, dImage.x, dImage.y, dImage.w, dImage.h);
            ctx.save();
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                ctx.font = text.font;
                ctx.fillStyle = text.color;
                ctx.fillText(text.text, text.x, text.y);
                ctx.fillStyle = button.color;
                ctx.fillRect(button.x, button.y, button.w, button.h);
                ctx.fillStyle = button.textColor;
                ctx.font = button.font;
                ctx.fillText(button.text, button.textX, button.textY);
            ctx.restore();
        };

        self.update = function(dy) {
            // if ( py >= opts.finY ) { return; }
            // if ( py >= half ) { self.half = true; }
            // py += dy;
            if ( rb.north.y >= opts.finY ) { return; }
            if ( rb.north.y >= half ) { self.half = true; }
            rhomb.update(dy, rb);
            dImage.y += dy;
            button.y += dy;
            button.textY += dy;
            text.y += dy;
        };

        self.setImage = function(image) {
            var dx = opts.side/2,
                dy = opts.side/2.6;
                // s = rb.east.x - rb.west.x;
            dImage.image = image;
            dImage.x = opts.x - dx;
            dImage.y = opts.y + dy;
            dImage.w = dx * 2;
            dImage.h = (dImage.w * image.height)/image.width;

            // TODO optimize animation

            // pCanvas = document.createElement('canvas');
            // pctx = pCanvas.getContext('2d');
            // pCanvas.width = s;
            // pCanvas.height = s;

            // px = rb.west.x;
            // py = rb.north.y;

            // rhomb.draw(pctx, rb);
            // pctx.drawImage(dImage.image, dImage.x, dImage.y, dImage.w, dImage.h);
            // pctx.textAlign = 'center';
            // pctx.textBaseline = 'middle';
            // pctx.font = text.font;
            // pctx.fillStyle = text.color;
            // pctx.fillText(text.text, text.x, text.y);
            // pctx.fillStyle = button.color;
            // pctx.fillRect(button.x, button.y, button.w, button.h);
            // pctx.fillStyle = button.textColor;
            // pctx.font = button.font;
            // pctx.fillText(button.text, button.textX, button.textY);
        };

        self.setColor = function(color) {
            rb.color = color;
        };

        self.checkButton = function(mouse) {
            return ( pointInRect (mouse, button) ) ? opts.text : false;
        };
    }

    function MainPanel(opts) {
        var self = this,
            rb = rhomb.set(opts.x, opts.y, opts.side, opts.color),
            ctx = opts.ctx,
            fs = Math.round(opts.side/4),
            font = fs + 'px futura',
            fontTJ = (fs * 1.2) + 'px futura-bol',
            fontS = (fs * 1.2) + 'px symbol1',
            fontTM = (fs * 0.6) + 'px futura-bol',
            y1 = rb.west.y - fs/2,
            y2 = rb.west.y + fs/2,
            trademark = String.fromCharCode(174),
            firstRow = getXs();

            function getXs() {
                var d, tj, tm, s, dx;
                ctx.save();
                    ctx.font = font;
                    d = ctx.measureText('Das ').width;
                    ctx.font = fontTM;
                    tm = ctx.measureText(trademark).width;
                    ctx.font = fontTJ;
                    tj = ctx.measureText('TOPJOB').width;
                    ctx.font = fontS;
                    s = ctx.measureText('@').width;
                ctx.restore();
                dx = (d + tj + tm + s)/2;
                return {
                    x0: opts.x - dx,
                    x1: opts.x - dx + d,
                    x2: opts.x - dx + d + tj + tm/2,
                    x3: opts.x - dx + d + tj + tm*1.5
                };
            }

        self.draw = function() {
            rhomb.draw(opts.ctx, rb);
            ctx.save();
                ctx.textBaseline = 'middle';
                ctx.font = font;
                ctx.fillStyle = '#FFF';
                ctx.fillText('Das ', firstRow.x0, y1);

                ctx.fillStyle = '#333';
                ctx.font = fontTJ;
                ctx.fillText('TOPJOB', firstRow.x1, y1);
                ctx.font = fontTM;
                ctx.fillText(trademark, firstRow.x2, y1 - fs * 0.3);

                ctx.font = fontS;
                ctx.fillText('@', firstRow.x3, y1 + fs * 0.05);

                ctx.textAlign = 'center';
                ctx.font = font;
                ctx.fillStyle = '#FFF';
                ctx.fillText('Produktprogramm', opts.x, y2);
            ctx.restore();
        };

        self.done = false;

        self.update = function(dy) {
            if ( rb.north.y >= opts.finY ) {
                self.done = true;
                return;
            }
            rhomb.update(dy, rb);
            y1 += dy;
            y2 += dy;
        };
    }

    self.run = function () {
        rAF = requestAnimationFrame(loop);
    };

    self.setImage = function(image) {
        deviceImage = image;
    };

    function update(dy) {
        for ( var i = 0; i < len; i++ ) {
            if ( i ) {
                if ( panels[i-1].half ) { panels[i].update(dy); }
            }else {
                panels[i].update(dy);
            }
        }
    }

    function draw() {
        for ( var i = 0; i < len; i++ ) {
            panels[i].draw();
        }
    }

    function loop() {
        clear();
        update(8);
        draw();
        if ( rAF ) {
            if ( panels[len-1].done ) {
                cancelAnimation();
                setEventListeners();
            }else {
                rAF = requestAnimationFrame(loop);
            }
        }
    }

    function clear() {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }

    function cancelAnimation() {
        cancelAnimationFrame(rAF);
        rAF = 0;
    }

    function setEventListeners() {
        ctx.canvas.addEventListener('mousemove', onMouseMovePP, false);
        ctx.canvas.addEventListener('click', onMouseClickPP, false);
    }

    function removeEventListeners() {
        ctx.canvas.removeEventListener('mousemove', onMouseMovePP, false);
        ctx.canvas.removeEventListener('click', onMouseClickPP, false);
    }

    function onMouseMovePP(e) {
        var mouse = getPointerPos(e);
        e.target.style.cursor = 'auto';
        for ( var i = 0; i < len; i++ ) {
            if ( i < len - 1 ) {
                panels[i].setColor('#E5E5E5');
                activeButton = panels[i].checkButton(mouse);
                if ( activeButton ) {
                    e.target.style.cursor = 'pointer';
                    panels[i].setColor('#D0D2D1');
                    break;
                }
            }
        }
        draw();
    }

    function onMouseClickPP() {
        if ( activeButton ) { window.alert('You\'re about to visit ' + activeButton); }
    }

}


},{}]},{},[1])