(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
/* jslint ignore:start */

function Counter(opts) {
	var self = this,
		step = -1,
		_step = -1,
		stages = [
			['CAGE CLAMP TECHNOLOGY', 0],
			['UNIVERSELLER LEITERANSCHLUSS', 0],
			['HOHE SICHERHEITSRESERVEN', 0],
			['MULTIFUNKTIONALES BRÜCKERSYSTEM', 0],
			['OMFORTABLE BESCHRIFTUNG', 0],
			['ALLE PRODUKTVORTEILE', 0]
		],
		len = stages.length,
		perStage = Math.floor(opts.frames/len),
		i = 0;

	for ( ; i < len; i++ ) {
		stages[i][1] = perStage * i;
	}
	self.step = function(n) {
		var n;
		_step = step;
		step += n;
		if ( step < -1 ) {
			step = _step;
		}else if ( step > opts.frames - 1 ) {
			step = _step;
		}else {
			checkStage();
			n = ( step < 0 ) ? 0 : step;
			opts.callback(n);
		}
	};

	function checkStage() {
		var i = 0;
		for ( ; i < len; i++ ) {
			if ( ( stages[i][1] === step && step > _step ) || ( stages[i][1] === _step && step <= _step ) ) {
				setStage(stages[i][0]);
				break;
			}
		}
	}

	function setStage(name) {
		var i = 0,
			len = opts.pictitems.length,
			elem, title, img;
		for ( ; i < len; i++ ) {
			elem = opts.pictitems[i];
			img = elem.querySelector('img');
			title = elem.getAttribute('title');
			if ( title === name ) {
				img.src = 'images/green-dot-32.png';
			}else {
				img.src = 'images/grey-circle-32.png';
			}
		}
	}
}

},{}]},{},[1])