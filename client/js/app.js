(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
/* jslint ignore:start */

var contactForm = document.getElementsByClassName('contact-form')[0],
	canvas = document.getElementById('page_canvas'),
    canvasOverlay = document.getElementsByClassName('canvas-overlay')[0],
	ctx = canvas.getContext('2d'),
	deviceImage = new Image(),
	geom = getGeometry(),
	initAnim = new InitAnimation(ctx, geom, onIAEnd),
	ppAnim = new PPAnimation(ctx, geom),
	cPage = new ContactPage(ctx, geom, onCFButtonClick),
	wheel = {
		wheelOn: false,
		on: function () {
			if ( !this.wheelOn ) {
				this.wheelOn = !this.wheelOn;
				document.body.addEventListener('mousewheel', onMouseWheel, false);
				document.body.addEventListener('DOMMouseScroll', onMouseWheel, false);
			}
		},
		off: function () {
			if ( this.wheelOn ) {
				this.wheelOn = !this.wheelOn;
				document.body.removeEventListener('mousewheel', onMouseWheel, false);
				document.body.removeEventListener('DOMMouseScroll', onMouseWheel, false);
			}
		}
	},
	customScroll = {
		allow: function (onlyscroll) {
			wheel.on();
			//if ( !onlyscroll ) { removeClass(scrollAliasBox, "hidden"); }
		},
		disallow: function () {
			wheel.off();
			//addClass(scrollAliasBox, "hidden");
		}
	},
    framess = new Frames({
        ctx: ctx,
        g: geom,
        framesForSlogan: 15, // frames
        stillState: 15, // frames
        disallowScroll: customScroll.disallow,
        allowScroll: customScroll.allow
    });

    framess = new Frames({
        ctx: ctx,
        g: geom,
        framesForSlogan: 15, // frames
        stillState: 15, // frames
//        textitems: menuStageItems,
//        pictitems: pictStageItems,
        disallowScroll: customScroll.disallow,
        allowScroll: customScroll.allow
    });
canvas.width = geom.w;
canvas.height = geom.h;
canvasOverlay.style.width = canvas.width + 'px';
canvasOverlay.style.height = canvas.height + 'px';

deviceImage.onload = function () {
	initAnim.setImage(deviceImage);
	loadImages(
		ctx,
		function () { initAnim.run(); },
		function (images) { framess.init(images); }
	);
};
deviceImage.src = 'client/images/misc/device.png';

//container.addEventListener("click", function(event) {
//	var et = event.target,
//		parent = et.parentNode,
//		text;
//	if ( et.tagName === "DIV" ) {
//		if ( hasClass(et, "menu-item") ) {
//			if ( hasClass(parent, "side-picts") ) {
//				text = et.getAttribute("title");
//			}else if ( hasClass(parent, "side-menu") ) {
//				text = et.textContent;
//			}
//			menuClickHandler(text);
//		}
//	}else if ( et.tagName === "IMG" ) {
//		parent = et.parentNode;
//		if ( hasClass(parent, "menu-item") ) {
//			text = parent.getAttribute("title");
//			menuClickHandler(text);
//		}
//	}
//}, false);


function menuClickHandler(t) {
	if ( t === 'DOWNLOADS' ) {
		downloadPresentation();
	}else {
		defaultActions();
		if ( t === 'KONTAKT' ) {
			showContactForm();
		}else if ( t === 'PRODUKTPROGRAMM' ) {
			launchPPAnimation();
		}else {
			framess.setStage(t);
		}

	}
// case 'CAGE CLAMP TECHNOLOGY': goToCheckpoint(t); break;
// case 'UNIVERSELLER LEITERANSCHLUSS': goToCheckpoint(t); break;
// case 'HOHE SICHERHEITSRESERVEN': goToCheckpoint(t); break;
// case 'MULTIFUNKTIONALES BRÜCKERSYSTEM': goToCheckpoint(t); break;
// case 'KOMFORTABLE BESCHRIFTUNG': goToCheckpoint(t); break;
// case 'ALLE PRODUKTVORTEILE': goToCheckpoint(t); break;

}

function defaultActions() {
	customScroll.disallow();
	if ( initAnim.running ) { initAnim.stop(); }
	addClass(contactForm, 'hidden');
	removeClass(document.body, 'bg-gradient');
	ppAnim.init();
	cPage.removeEventListeners();
	// ppAnim.removeEventListeners();
	framess.resetMenu();
}

function launchPPAnimation() {
	addClass(document.body, 'bg-gradient');
	ppAnim.run();
}

function showContactForm() {
	removeClass(container, 'hidden');
	removeClass(contactForm, 'hidden');
	addClass(document.body, 'bg-gradient');
	cPage.init();
}

function downloadPresentation() {
	var link = document.createElement('a');
	link.href = 'misc/WAGO_screen_phase_2_150323_jk.pptx';
	link.setAttribute('download', 'presentation.pptx');
	document.body.appendChild(link);
	link.click();
	link.parentNode.removeChild(link);
}

function onIAEnd() {
	//removeClass(container, "hidden");
	//mo.click();
	customScroll.allow();
	//scrollAlias.addEventListener("click", onSCMouseClick, false);
}

function onMouseWheel(e) {
	var delta = -1 * Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
	if ( initAnim.running ) { initAnim.stop(); }
	framess.step(delta);
	return false;
}

function onSCMouseClick() {
	if ( initAnim.running ) { initAnim.stop(); }
	framess.nextStage();
}

function onCFButtonClick(btn) {
	var checkbox = document.getElementById('cfBitte');
	if ( btn === 'Kontakt aufnehmen' ) {
		removeClass(contactForm, 'muster-form');
		checkbox.checked = false;
	}else {
		addClass(contactForm, 'muster-form');
		checkbox.checked = true;
	}
}


},{}]},{},[1])