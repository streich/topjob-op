'use strict';
/* jslint browser:true */
/* global TweenMax, TimelineMax */

var debug = require('debug')('wago:scenes');
var ScrollMagic = require('./scrollMagic');

/**
 * Setup for ScrollMagic scenes
 */
module.exports = function setupScenes(app) {
    var end, duration;

    duration = app.sm.framesEarly * app.ratio;

    debug('load scenes');

    new ScrollMagic.Scene({
            offset: app.stages.moreInfo.mainFrame * app.ratio,
            duration: duration,
        })
        .setTween('#page_canvas, .canvas-container', {
                top: '-=' + duration,
                ease: 'linear'
            })
        .addTo(app.sm.controller);

    function registerStage(key, paneClass) {
        end = app.stages[key].mainFrame * app.ratio;

        new ScrollMagic.Scene({
                offset: end - duration,
                duration: duration,
                // reverse: false,
            })
            .setTween('.pane--' + paneClass, {
                    left: '0',
                })
            //.addIndicators({ name: '#' + key })
            .setPin('#' + key)
            .addTo(app.sm.controller);

        new ScrollMagic.Scene({
                offset: end - duration,
                duration: duration,
                // reverse: false,
            })
            .setTween('#' + key + ' .js-sm-title, #' + key + ' .js-sm-body', {
                    y: '0',
                    x: '0',
                })
            // .addIndicators({name: '#' + key})
            .addTo(app.sm.controller);
    }


    function registerFadeScene(begin, end, key, tweens) {
        var visibleDuration  = (end - begin) * app.ratio;
        var offset = begin * app.ratio;

        var tween = new TimelineMax({useFrames: true});

        for(var i = 0; i < tweens.length; i++) {
            var fadeDuration = (tweens[i].end - tweens[i].start) * app.ratio;
            tween.add(TweenMax.to('#' + tweens[i].elementId, fadeDuration, {opacity: 1}));
        }

        new ScrollMagic.Scene({
            offset: offset,
            duration: visibleDuration,
        })
        .setTween(tween)
        .setPin('#' + key + 'FadeExtras')
        //.addIndicators({name: 'fade in'})
        .addTo(app.sm.controller);
    }

    function registerMoreInfoFades() {
        var pinDuration = (app.stages.moreInfo.endFrame  - app.stages.moreInfo.mainFrame) * app.ratio;
        var visibleDuration = (app.stages.moreInfo.mainFrame - 2) * app.ratio;
        var offset = (app.stages.moreInfo.mainFrame - 0.5) * app.ratio;

        var tween = TweenMax.to('#alle', visibleDuration, {opacity: 100});

        new ScrollMagic.Scene({
            offset: offset,
            duration: pinDuration
        })
        .setTween(tween)
        .setPin('#alle')
        //.addIndicators({name: 'opacity 100'})
        .addTo(app.sm.controller);
    }

    registerStage('clamp', 'clamp');
    registerStage('universeller', 'leiterarten');
    registerStage('hohe', 'hohe');
    registerStage('multifunktionales', 'multifunktionales');
    registerStage('komfortable', 'komfortable');

    var leiterartenArray = [];
    leiterartenArray.push({start:  77, end:  81, elementId: 'leiterartenTitle'});
    leiterartenArray.push({start:  81, end:  86, elementId: 'eindrahtige'});
    leiterartenArray.push({start:  90, end:  97, elementId: 'mehrdrahtige'});
    leiterartenArray.push({start:  99, end: 104, elementId: 'feindrahtige'});
    leiterartenArray.push({start: 104, end: 112, elementId: 'leiter'});
    registerFadeScene(66, 114, 'leiterarten', leiterartenArray);

    registerMoreInfoFades();


};
