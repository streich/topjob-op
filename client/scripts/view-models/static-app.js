'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:vms:app');

var $ =
window.jQuery =
window.$ = require('jquery');

var Lang = require('../language'),
    utils = require('../utils');

require('../components/info-modal');
require('../components/contact-form');
require('../components/pane-intro');
require('../components/pane-clamp');
require('../components/pane-intro-stop');
require('../components/pane-universeller');
require('../components/pane-hohe');
require('../components/pane-multifunktionales');
require('../components/pane-komfortable');
require('../components/pane-alle');

require('../components/pane-moreInfo');
require('../components/pane-contact');
require('../components/pane-diamonds');
require('../components/side-menu');
require('../components/lang-switch');

var Vue = require('vue');

function preventDefault(e) {
    if (e.preventDefault)
        e.preventDefault();

    if (e.stopPropagation)
        e.stopPropagation();

    e.returnValue = false;

    // ------

    if (window.event) {
        e = window.event;

        if (e.preventDefault)
            e.preventDefault();

        if (e.stopPropagation)
            e.stopPropagation();

        e.returnValue = false;
    }
}


module.exports = function(selector) {
    selector = selector || '.js-vm-app';

    return new Vue({
        el: selector,

        data: {
            isStatic: true,
            isLoaded: false,
            isMenuOpen: false,
            isMenuHidden: false,
            isHeaderOpen: false,
            isContactFormVisible: false,
            isLangSwitchOpen: false,
            isScrollPaused: false,
            isOverlayHidden: false,
            isIgnoringPin: false,
            isIgnoringUserScroll: false,
            isSkipping: false,

            heightReducer: 380,

            pane: '',
            ratio: 0,
            geometry: utils.getGeometry(),
            frames: [],
            video: {
                w: 0,
                h: 0,
            },

            lang: {
                header: {},
                intro: {},
                introstop: {
                    aside: {},
                },
                clamp: {},
                leiterarten: {},
                hohe: {},
                bruckerprogramm: {},
                komfortable: {},
                alle: {},
                downloads: {},
                footer: {
                    links: {},
                },
                produktprogramm: {},
                contact: {
                    fields: {},
                },
                sideMenu: {
                    clamp: {},
                    leiterarten: {},
                    hohe: {},
                    bruckerprogramm: {},
                    schnelle: {},
                    produktvorteile: {},
                    contact: {},
                    products: {},
                    downloads: {},
                },
                static: {
                    imageAnchors: {}
                },
                scrollAlias: {},
                modal: {},
            },
        },

        computed: {
            frameDimensions: {
                get: function() {
                    var aspectRatio = 16/9;
                    var canvasAspectRatio = this.geometry.w / this.geometry.h;
                    var height, width, yDiff = 0;

                    if (aspectRatio > canvasAspectRatio) {

                        height = this.geometry.h;
                        width =  16 * (height / 9);
                        yDiff = 0;

                    } else if (aspectRatio < canvasAspectRatio) {

                        width =  this.geometry.w;
                        height = 9 * (width / 16);
                        yDiff = (this.geometry.h - height) / 2;

                    } else {

                        width =  this.geometry.w;
                        height = this.geometry.h;

                    }

                    return {
                        width: width,
                        height: height,
                        diff: yDiff,
                    };
                }
            },
        },

        created: function() {
        },

        ready: function() {
            var self = this;

            var lang = new Lang('client/language/lang.xml');
            lang.load(function(keys) {
                this.lang = keys;
                this.isLoaded = true;
                $('.lang-toggle div').attr('class','flag-icon flag-icon-'+this.lang.lang.current);
            }.bind(this));

            self.video.w = $('.__video').width();
            self.video.h = $('.__video').height();

            var locationHash = window.location.hash;
            if(locationHash != '') {
                locationHash = locationHash.replace('#', '');
                this.setPane(locationHash);
            }

            $('.js-sm-title, .js-sm-body').css({
                transform: 'none'
            });

            $(window).on('resize', function() {
                self.video.w = $('.__video').width();
                self.video.h = $('.__video').height();
                self.geometry = utils.getGeometry();
                self.alignPopouts();

                $('.js-sm-title, .js-sm-body').css({
                    transform: 'none'
                });

            });



            $('body').addClass('loaded');
            $(window).on('resize', function() {
                 _setBreakpoints();
            });

            function _setBreakpoints(){
               var width = $(document).width(),
                   $body = $('body');

               $body.removeClass("breakpoint-m breakpoint-s");
               if(width <= 992 ){
                   $body.addClass("breakpoint-m");
               }
                if(width <= 768 ){
                   $body.addClass("breakpoint-s");
               }

            }
            _setBreakpoints();

            $('.modal__scrollToTop').click(function(){
              $('.info__modal').animate({
                scrollTop: 0
              }, 500);
            });

              $('.site-header').click(function(e){
                e.stopPropagation();
              });

              $('body').click(function(e){
                $('.site-header').removeClass('open');
                 $('.navbar-langSwitch-wrapper').removeClass('open');
              });

              $('.navbar-toggle').on('click',function(){
                 $('.site-header').toggleClass('open');
                 if(  $('.site-header').hasClass('open') &&  $('.navbar-langSwitch-wrapper').hasClass('open') ){
                   $('.navbar-langSwitch-wrapper').removeClass('open');
                 }
              });
              $('.lang-toggle').on('click',function(){
                 if(  $('.site-header').hasClass('open') &&  $('body').hasClass('breakpoint-m') ){
                   $('.site-header').removeClass('open');
                 }
              });
              // Set handlers for opening/closing the header
              // $('.site-header__logo').mouseover(function() {
              //    if(!$('body').hasClass('breakpoint-m')) $('.site-header').addClass('open');
              // });

              // $('.site-header').mouseleave(function() {
              // if(!$('body').hasClass('breakpoint-m') && !$('.navbar-langSwitch-wrapper').hasClass('open'));  $('.site-header').removeClass('open');
              // });


            $(document).on('click','.site-header a',function() {
            if($('body').hasClass('breakpoint-m'))  $('.site-header').removeClass('open');
            });


            $('.scroll-maker').css({ display: 'none' });



            $('.pane').css({ left: 'auto' });

            $('.video-selector__item').click(function() {

                $(this).siblings().removeClass('active');
                $(this).addClass('active');

                var pane = $(this).data('pane');

                switch (pane) {
                    case 'clamp':
                        window.location.hash = 'cage-clamp-technology';
                        window._player.seek(0.01);
                        break;

                    case 'leiterarten':
                        window.location.hash = 'alle-leiterarten';
                        window._player.seek(16.9);
                        break;

                    case 'hohe':
                        window.location.hash = 'hohe-sicherheitsreserven';
                        window._player.seek(29.2);
                        break;

                    case 'multifunktionales':
                        window.location.hash = 'multifunktionales-brückersystem';
                        window._player.seek(43.7);
                        break;

                    case 'komfortable':
                        window.location.hash = 'schnelle-beschriftung';
                        window._player.seek(65);
                        break;
                }

            });

            window._player.onTime(function(e) {
                if (e.position < 16.4) {
                    self.pane = 'clamp';
                    $('.video-selector__item[data-pane=clamp]')
                        .addClass('active')
                        .siblings().removeClass('active');

                } else if (e.position < 29) {
                    self.pane = 'universeller';
                    $('.video-selector__item[data-pane=leiterarten]')
                        .addClass('active')
                        .siblings().removeClass('active');

                } else if (e.position < 43.7) {
                    self.pane = 'hohe';
                    $('.video-selector__item[data-pane=hohe]')
                        .addClass('active')
                        .siblings().removeClass('active');

                } else if (e.position < 65) {
                    self.pane = 'multifunktionales';
                    $('.video-selector__item[data-pane=multifunktionales]')
                        .addClass('active')
                        .siblings().removeClass('active');

                } else if (e.position < 74) {
                    self.pane = 'komfortable';
                    $('.video-selector__item[data-pane=komfortable]')
                        .addClass('active')
                        .siblings().removeClass('active');

                }
            });

            window._player.onComplete(function() {
                $(document.body).animate({scrollTop: 710});
                // if ($(window).scrollTop < 742);
                // self.setPane('moreInfo');
            });



        },

        methods: {

            toggleModal: function(checkpoint) {
                var modal = window._modals[0];
                this.lang.modal = this.lang[checkpoint].modal;

                if(modal && modal.toggle) {
                    modal.toggle();
                }
            },

            closeAllModals: function() {
                debug('closing all modals');

                for (var i = 0; i < window._modals.length; i++) {
                    var modal = window._modals[i];

                    if (modal && modal.close)
                        modal.close();
                }
            },

            showMoreInfo: function() {
                this.closeAllModals();
                window.location.href = '#alle-produktvorteile';
                this.isMenuOpen = false;
            },

            showContact: function() {
                this.closeAllModals();
                window.location.href = '#kontakt';
                this.active = 'kontakt';
                this.isMenuOpen = false;
                this.clearPane();
            },

            showProductProgram: function() {
                this.closeAllModals();
                window.location.href = '#produktprogramm';
                this.active = 'produktprogramm';
                this.isMenuOpen = false;
                this.clearPane();
            },

            showDownloads: function() {
                this.closeAllModals();
                window.location.href = '#downloads';
                this.active = 'downloads';
                this.isMenuOpen = false;
                this.clearPane();
            },

            seek: function(time) {
                window._player.seek(time);

                setTimeout(function() {
                    window._player.pause();
                }, 0);
            },
            setPaneOrSkipTo: function(pane, hash) {
                if (this.isStatic)
                    this.setPane(pane);
                else
                    this.skipTo(this.stages[pane].mainFrame, hash);
            },
            setPane: function(pane) {

                switch (pane) {
                    case 'clamp':
                    case 'cage-clamp-technology':
                        this.pane = 'clamp';
                        window.location.hash = 'cage-clamp-technology';
                        this.seek(6.9);
                        $(document.body).animate({scrollTop: 0});
                        break;

                    case 'universeller':
                    case 'alle-leiterarten':
                        this.pane = 'universeller';
                        window.location.hash = 'alle-leiterarten';
                        this.seek(16.9);
                        $(document.body).animate({scrollTop: 0});
                        break;

                    case 'hohe':
                    case 'hohe-sicherheitsreserven':
                        this.pane = 'hohe';
                        window.location.hash = 'hohe-sicherheitsreserven';
                        this.seek(29.2);
                        $(document.body).animate({scrollTop: 0});
                        break;

                    case 'multifunktionales':
                    case 'multifunktionales-brückersystem':
                        this.pane = 'multifunktionales';
                        window.location.hash = 'multifunktionales-brückersystem';
                        this.seek(43.7);
                        $(document.body).animate({scrollTop: 0});
                        break;

                    case 'komfortable':
                    case 'schnelle-beschriftung':
                        this.pane = 'komfortable';
                        window.location.hash = 'schnelle-beschriftung';
                        this.seek(65);
                        $(document.body).animate({scrollTop: 0});
                        break;

                    case 'moreInfo':
                        this.pane = 'moreInfo';
                        window.location = window.location.pathname + '#alle';
                        break;
                }
            },

            clearPane: function() {
                this.pane = '';
            },

            alignPopouts: function() {
                // Re-position pop-out labels
                var leiter = $('.__toggle-2 .popout__label');
                var sicher = $('.__toggle-5 .popout__label');
                var besh = $('.__toggle-4 .popout__label');

                var leiterDimensions = leiter.get(0).getBoundingClientRect();
                var sicherDimensions = sicher.get(0).getBoundingClientRect();
                var beshDimensions = besh.get(0).getBoundingClientRect();
                sicher.css({ right: '-=' + (leiterDimensions.left - sicherDimensions.left) });
                besh.css({ right: '-=' + (leiterDimensions.left - beshDimensions.left) });
            },

        },
    });
};
