'use strict'; /* jslint browser:true */
/* global _gaq */
var debug = require('debug')('wago:vms:app'),
    sdbg = require('debug')('wago:vms:app/scroll'),
    speed = require('debug')('wago:vms:app/speed');

var $ =
window.jQuery =
window.$ = require('jquery');

var browserDetection = require('../../vendor/browserdetection/src/browser-detection');

var initializer = require('../initializer'),
    utils = require('../utils'),
    Lang = require('../language');

var fitImageOn = require('../../../lib/fitImageOn');
var ScrollMagic = require('../scrollMagic');

require('../components/lang-switch');
require('../components/side-menu');
require('../components/scroll-alias');
require('../components/info-modal');
require('../components/contact-form');
require('../components/pane-intro');
require('../components/pane-clamp');
require('../components/pane-intro-stop');
require('../components/pane-universeller');
require('../components/pane-hohe');
require('../components/pane-multifunktionales');
require('../components/pane-komfortable');
require('../components/pane-alle');

require('../components/pane-moreInfo');
require('../components/pane-contact');
require('../components/pane-diamonds');

var Vue = require('vue');

function preventDefault(e) {
    if (e.preventDefault)
        e.preventDefault();

    if (e.stopPropagation)
        e.stopPropagation();

    e.returnValue = false;

    // ------

    if (window.event) {
        e = window.event;

        if (e.preventDefault)
            e.preventDefault();

        if (e.stopPropagation)
            e.stopPropagation();

        e.returnValue = false;
    }
}


module.exports = function(selector) {
    selector = selector || '.js-vm-app';

    return new Vue({
        el: selector,

        data: {
            isLoaded: false,
            isMenuOpen: true,
            isMenuHidden: true,
            isHeaderOpen: false,
            isLangSwitchOpen: false,
            isContactFormVisible: false,
            isScrollPaused: false,
            isOverlayHidden: true,
            isIgnoringPin: false,
            isIgnoringUserScroll: false,
            isSkipping: false,
            targetPosition: 0,

            scrollTimeout: 12,
            scrollRate: 215,

            animation: {
                fps: 60,
                then: Date.now(),
                now: null,
                delta: null,
            },

            ignoreScroll: false,
            pane: 'intro',
            active: '',
            ratio: 75,
            timeoutId: null,
            scrollTimeoutId: null,
            activePane: null,
            upcomingStage: null,

            bodyScrollTop: 0,

            canvas: document.getElementById('page_canvas'),
            canvasOverlay: document.getElementsByClassName('canvas-overlay')[0],
            ctx: null,

            geometry: utils.getGeometry(),
            frames: [],
            stages: {
                intro:              { beginFrame:   0, mainFrame:   0, endFrame:   0 },
                introstop:          { beginFrame:   0, mainFrame:  19, endFrame:  26 },
                clamp:              { beginFrame:  27, mainFrame:  58, endFrame:  65 },
                universeller:       { beginFrame:  66, mainFrame: 114, endFrame: 121 },
                hohe:               { beginFrame: 122, mainFrame: 180, endFrame: 187 },
                multifunktionales:  { beginFrame: 188, mainFrame: 272, endFrame: 279 },
                komfortable:        { beginFrame: 280, mainFrame: 340, endFrame: 347 },
                moreInfo:           { beginFrame: 348, mainFrame: 354, endFrame: 360 },
            },
            fadeExtras: {
                leiterarten: {begin: 66, end: 114}
            },
            offset: 7,

            scene: {
                currentFrame: 0,
                targetFrame: 0,
                lastFrame: 0,
                rafId: 0,
                isAnimating: false
            },

            sm: {
                framesEarly: 5, // How many frames early to start easing in the elements
                controller: new ScrollMagic.Controller(),
                scenes: [],
            },

            lang: {
                header: {},
                intro: {},
                introstop: {
                    aside: {},
                },
                clamp: {},
                leiterarten: {},
                hohe: {},
                bruckerprogramm: {},
                komfortable: {},
                alle: {},
                downloads: {},
                footer: {
                    links: {},
                },
                produktprogramm: {},
                contact: {
                    fields: {},
                },
                sideMenu: {
                    clamp: {},
                    leiterarten: {},
                    hohe: {},
                    bruckerprogramm: {},
                    schnelle: {},
                    produktvorteile: {},
                    contact: {},
                    products: {},
                    downloads: {},
                },
                static: {
                    imageAnchors: {}
                },
                scrollAlias: {},
                modal: {},
            },
            browserDetect: null,
        },

        computed: {
            scroll: {
                get: function() {
                    return this.bodyScrollTop;
                },
                set: function(newValue) {
                    if (newValue > (this.stages.introstop.mainFrame * this.ratio)) {
                        this.$.scrollAlias.isTextHidden = true;
                        this.$.scrollAlias.isArrowDown = true;
                    } else {
                        this.$.scrollAlias.isTextHidden = false;
                        this.$.scrollAlias.isArrowDown = false;
                    }

                    this.scrollTop = newValue;
                    this.bodyScrollTop = newValue;
                },
            },

            scrollTop: {
                get: function() {
                    // return $('.scroller').get(0).scrollTop;

                    if (document.documentElement &&
                        document.documentElement.scrollTop)
                        return document.documentElement.scrollTop;

                    if (document.body.parentNode &&
                        document.body.parentNode.scrollTop)
                        return document.body.parentNode.scrollTop;

                    if (document.body &&
                        document.body.scrollTop)
                        return document.body.scrollTop;

                    return 0;
                },
                set: function(newValue) {
                    // $('.scroller').get(0).scrollTop = newValue;

                    if (document.documentElement)
                        document.documentElement.scrollTop = newValue;

                    if (document.body.parentNode)
                        document.body.parentNode.scrollTop = newValue;

                    if (document.body)
                        document.body.scrollTop = newValue;
                },
            },

            frameDimensions: {
                get: function() {
                    var aspectRatio = 16/9;
                    var canvasAspectRatio = this.geometry.w / this.geometry.h;
                    var height, width, yDiff = 0;

                    if (aspectRatio > canvasAspectRatio) {

                        height = this.geometry.h;
                        width =  16 * (height / 9);
                        yDiff = 0;

                    } else if (aspectRatio < canvasAspectRatio) {

                        width =  this.geometry.w;
                        height = 9 * (width / 16);
                        yDiff = (this.geometry.h - height) / 2;

                    } else {

                        width =  this.geometry.w;
                        height = this.geometry.h;

                    }

                    return {
                        width: width,
                        height: height,
                        diff: yDiff,
                    };
                }
            },
        },

        created: function() {
            var wheel = (function(me) {
                var timedout = false;

                me.$on('animation-finished', function() {
                    timedout = false;
                });

                return function wheel(e) {
                    preventDefault(e);

                    if (!timedout &&
                        !me.isScrollPaused &&
                        // !me.ignoreScroll &&
                        !me.isIgnoringUserScroll) {

                        me.isMenuOpen = false;
                        me.isHeaderOpen = false;

                        if (e.deltaY < 0)
                            me.scrollTo(me.scrollTop - me.scrollRate);
                        else if (e.deltaY > 0)
                            me.scrollTo(me.scrollTop + me.scrollRate);

                        timedout = true;
                        setTimeout(function() { timedout = false; }, me.scrollTimeout);
                    }
                };
            })(this);

            window.addWheelListener(window, wheel, false);
            window.addWheelListener(document, wheel, false);

            if (document.documentElement)
                window.addWheelListener(document.documentElement, wheel, false);

            if (document.body.parentNode)
                window.addWheelListener(document.body.parentNode, wheel, false);

            if (document.body)
                window.addWheelListener(document.body, wheel, false);
        },

        ready: function() {

            var self = this;
            debug('ready');

            var lang = new Lang('client/language/lang.xml');
            lang.load(function(keys) {
                console.log(keys);
                this.lang = keys;
                this.isLoaded = true;
                $('.lang-toggle div').attr('class','flag-icon flag-icon-'+this.lang.lang.current);
            }.bind(this));


            // Setup scroll-magic
            require('../scenes')(this);

            this.scroll = 0;

            this.ctx = this.canvas.getContext('2d');
            this.isLoaded = true;
            this.isOverlayHidden = false;

            initializer.ctx = this.ctx;

            initializer.$on('images-initialized', function(images) {
                self.frames = images;
                self.scene.lastFrame = self.frames.length - 1;
                self.scroll = 0;
            });

            self.browserDetect = browserDetection();
            if(self.browserDetect.browser == 'ie') {
                this.scrollTimeout = 0;
                this.scrollRate = 900;
            }

            initializer.$on('initial-frames-ready', function() {
                // Re-render everything when the page is resized
                $(window).on('resize', function() {
                    debug('handle resize');
                    self.geometry = utils.getGeometry();

                    debug('render frame: %s', self.scene.currentFrame,
                        self.ctx.canvas.width, self.ctx.canvas.height,
                        self.frames[self.scene.currentFrame].small);

                    setTimeout(function() {
                        self.showFrame(self.scene.currentFrame);
                        self.alignPopouts();
                    }, 0);
                });

                $('body').addClass('loaded');

                $(window).on('resize', function() {
                     _setBreakpoints();
                });

                function _setBreakpoints(){
                   var width = $(document).width(),
                       $body = $('body');

                   $body.removeClass("breakpoint-m breakpoint-s");
                   if(width <= 992 ){
                       $body.addClass("breakpoint-m");
                   }
                    if(width <= 768 ){
                       $body.addClass("breakpoint-s");
                   }

                }
                _setBreakpoints();



                  $('.site-header').click(function(e){
                    e.stopPropagation();
                  });

                  $('body').click(function(e){
                    $('.site-header').removeClass('open');
                     $('.navbar-langSwitch-wrapper').removeClass('open');
                  });

                  $('.navbar-toggle').on('click',function(){
                     $('.site-header').toggleClass('open');
                     if(  $('.site-header').hasClass('open') &&  $('.navbar-langSwitch-wrapper').hasClass('open') ){
                       $('.navbar-langSwitch-wrapper').removeClass('open');
                     }
                  });
                  $('.lang-toggle').on('click',function(){
                     if(  $('.site-header').hasClass('open') &&  $('body').hasClass('breakpoint-m') ){
                       $('.site-header').removeClass('open');
                     }
                  });
                  // Set handlers for opening/closing the header
                  // $('.site-header__logo').mouseover(function() {
                  //    if(!$('body').hasClass('breakpoint-m')) $('.site-header').addClass('open');
                  // });

                  // $('.site-header').mouseleave(function() {
                  // if(!$('body').hasClass('breakpoint-m') && !$('.navbar-langSwitch-wrapper').hasClass('open'));  $('.site-header').removeClass('open');
                  // });


                $(document).on('click','.site-header a',function() {
                if($('body').hasClass('breakpoint-m'))  $('.site-header').removeClass('open');
                });

                self.alignPopouts();

                for(var key in self.stages) {
                    self.loadFrames(self.stages[key].mainFrame, 5, 5);
                }

                var locationHash = window.location.hash;
                if(locationHash != '') {
                    var goToFrame = self.getFrameFromHash(locationHash);
                    self.skipTo(goToFrame);
                    self.activateNavs(0, true);
                } else {
                    // Behold, the hellish setTimeout chain and weep.
                    setTimeout(function() {
                        $('.spotlightBg').fadeIn(1000);
                    });

                    setTimeout(function() {
                        self.scrollToFrame(self.stages.introstop.mainFrame);

                        setTimeout(function() {
                            self.activateMouseWheel();
                        }, 0);

                        $('.spotlightBg').hide();
                        $('.intro-container').fadeOut(100);

                         setTimeout(function() {
                        }, 1000);

                         $('.logo-text-lg, aside').fadeIn(2000);
                        self.activateNavs(1000, true);
                    }, 1100);
                }

            });

            initializer.initialize(this.stages.introstop.endFrame);

            this.$on('animation-scroll', function(frame) {
                if (self.isScrollPaused) return;
                debug('$on - animation-scroll');

                if (frame > self.stages.introstop.mainFrame) {
                    self.headerOpen = false;
                }

                var stage = self.updateActivePane();

                // Show the intro again, if we scroll back to the first frame
                if (frame == 0) self.fadeIntro();

                self.showFrame(frame);

                // If a checkpoint is scrolled to lock scrolling in place for a
                // time so that that checkpoint can be seen in all its naked
                // glory.
                speed('frame %s : stage %s', frame, stage.mainFrame);
                if (frame === stage.mainFrame && self.scrollTop <= 27601) {
                    if (!self.isIgnoringScroll && !self.isIgnoringPin) {

                        speed('scrolling paused at %s', frame);
                        self.isScrollPaused = true;

                        // Reset scroll position to the locked frame
                        setTimeout(function() {
                            var scroll = stage.mainFrame * self.ratio;
                            self.bodyScrollTop = scroll;
                            self.scroll = scroll;
                            self.scrollTop = scroll;
                        }, 0);

                        // After a time, release scrolling
                        setTimeout(function() {
                            self.isIgnoringUserScroll = false;
                            self.isScrollPaused = false;
                            self.ignoreScroll = false;

                            self.$.scrollAlias.isHidden = false;
                            speed('scrolling un-paused from %s', frame);

                            self.isIgnoringPin = true;
                            $(window).one('scroll', function() {
                                setTimeout(function() {
                                    self.isIgnoringPin = false;
                                }, 500);
                            });
                        }, 2000);

                    }

                }
            }.bind(this));

            this.$on('animation-finished', function(isCheckpoint) {
                // if (inertia) {
                //     self.inertFrame(2 * (isForward ? -1 : 1));
                // }

                // Re-allow manual scrolling
                setTimeout(function() {
                    self.isSkipping = false;
                    self.ignoreScroll = false;
                }, 600);

                if (self.isIgnoringUserScroll) {
                    setTimeout(function() {
                        self.isIgnoringUserScroll = false;
                    }, 2000);
                }

                clearTimeout(self.scrollTimeoutId);

                // I'm not really sure what is going on here
                // Something to do with re-revealing the scroll alias... :\
                if(self.stages[self.pane].mainFrame == self.scene.currentFrame) {
                    setTimeout(function() {
                        self.activateScrollAlias(isCheckpoint);
                    }, 1000);
                } else {
                    self.scrollTimeoutId = setTimeout(function() {
                        self.activateScrollAlias();
                    }, 8000);
                }
            }.bind(this));
        },

        methods: {
            fadeIntro: function() {
                $('.device-image, .logo-text-lg, aside').fadeIn(2000);
            },

            activateNavs: function (timeout, isHovering) {
                setTimeout(function() {
                    this.isMenuHidden = false;
                    this.isHeaderOpen = true;
                    this.activateScrollAlias(true);
                }.bind(this), timeout);
            },

            activateScrollAlias: function(isHovering) {
                this.$.scrollAlias.isHidden = false;
                this.$.scrollAlias.isArrowDown = true;

                if(isHovering) {
                    // Let the scroll button hover for a bit.
                    this.$.scrollAlias.isHovering = true;
                    setTimeout(function() {
                        this.$.scrollAlias.isHovering = false;
                    }.bind(this), 1000 * 2.75 * 3); // Time for one "bounce" * 3
                }
            },

            toggleModal: function(checkpoint) {
                var modal = window._modals[0];
                this.lang.modal = this.lang[checkpoint].modal;

                this.trackMoreInfo(checkpoint);

                if(modal && modal.toggle) {
                    modal.toggle();
                }
            },

            activateMouseWheel: function() {
                debug('activate scroll');

                function onScroll(me) {

                    var scroll, oldScroll;

                    return function handler() {

                        if (me.isSkipping) {
                            return;
                        }

                        if (me.isScrollPaused) {
                            debug('scrolling paused at %s', me.scroll);
                            me.scrollTop = me.scroll;
                        }

                        oldScroll = scroll;
                        scroll = me.scrollTop;
                        var diff = (scroll - oldScroll);
                        var isForward = diff >= 0;
                        diff = Math.abs(diff);

                        if (scroll > 37601) {
                            return;
                        }

                        speed('diff: %s', diff);

                        // if (diff > me.ratio) {
                        //     me.scrollTop = oldScroll + (isForward ? me.ratio : -me.ratio);
                        //     return;
                        // }

                        if (scroll > (me.stages.introstop.mainFrame * me.ratio)) {
                            me.$.scrollAlias.isTextHidden = true;
                            me.$.scrollAlias.isArrowDown = true;
                        } else {
                            me.$.scrollAlias.isTextHidden = false;
                            me.$.scrollAlias.isArrowDown = false;
                        }

                        me.updateActivePane(true);

                        if (!me.ignoreScroll) {
                            if (me.scrollTop > 27601) {
                                me.showFrame(me.scene.lastFrame);
                                return;
                            }

                            // me.scrollTop = oldScroll;
                            me.isMenuOpen = false;

                            // Get the next upcoming stage
                            var stage;
                            sdbg('is forward: %s', isForward);
                            for (var key in me.stages) {
                                var _stage = me.stages[key];
                                if ((isForward && scroll < _stage.mainFrame * me.ratio) ||
                                   (!isForward && scroll > _stage.mainFrame * me.ratio)) {
                                    stage = _stage;
                                    if (isForward) break;
                                }
                            }

                            var capture;
                            if (diff > 250)
                                capture = 120;
                            else if (diff > 180)
                                capture = 60;
                            else if (diff > 90)
                                capture = 30;
                            else
                                capture = 20;

                            capture *= me.ratio;
                            // if (true) {
                            if (stage &&
                                oldScroll !== scroll &&
                                Math.abs(stage.mainFrame * me.ratio - scroll) <= capture) {

                                // me.ignoreScroll = true;
                                if (!me.isIgnoringPin)
                                    me.isIgnoringUserScroll = true;

                                me.scrollToFrame(stage.mainFrame);

                            } else {
                                me.scrollTo(scroll, true, true);

                            }
                        } else {
                            debug('ignore scroll event');
                            var targetFrame = Math.ceil(scroll / me.ratio);

                            if(!me.frames[targetFrame].small._isLoaded) {
                                window.cancelAnimationFrame(me.scene.rafId);
                                me.scene.rafId = null;

                                me.frames[targetFrame].small.addEventListener('load', function() {
                                    me.scrollTo(me.targetPosition);
                                    // me.scrollToFrame(me.scene.targetFrame);
                                });
                            }
                        }
                    };
                }

                this.scrollTop = 0;
                window.addEventListener('scroll', onScroll(this), false);
            },

            inertFrame: function(frames) {
                frames = frames || 1;
                this.inert(frames * this.ratio);
            },

            inert: function(distance) {
                distance = distance || 1;
                this.scrollTo(this.scroll + distance);
            },

            scrollNext: function() {
                this.closeAllModals();
                this.isMenuOpen = false;

                // Find the next frame
                var frames = [];
                for (var key in this.stages) {
                    var stage = this.stages[key];
                    if (this.scene.currentFrame < stage.mainFrame) {
                        frames.push(stage.mainFrame);
                    }
                }

                frames = frames.sort(this.numericSort);


                // TODO: Remove the magic numbers and use real references
                // Scroll to the set frame
                if (!frames[0] && this.scroll >= 26550) {
                    debug('scroll to next: contact');
                    this.trackScrollNext(null, 'kontakt');
                    this.showContact();
                } else {
                    debug('scroll to next: %s', frames[0]);
                    this.trackScrollNext(frames[0]);
                    this.scrollToFrame(frames[0]);
                }
            },

            numericSort: function(a, b) {
                return a - b;
            },

            loadFrames: function(n, after, before) {
                // Set defaults
                if (!after && after !== 0)      after = 20;
                if (!before && before !== 0)    before = 5;

                var i;

                var cancel = false;
                // Load frames after the target
                for (i = n; i < Math.min(n+after, this.scene.lastFrame); i++) {
                    if (!this.frames[i].small._isLoading &&
                        !this.frames[i].small._isLoaded) {
                        this.frames[i]._load();
                    }
                }

                // Load frames before the target
                for (i = n; i > Math.max(n-before, 0); i--) {
                    if (!this.frames[i].small._isLoading &&
                        !this.frames[i].small._isLoaded) {
                        this.frames[i]._load();
                    }
                }
            },

            updateActivePane: function(keepStage, n) {
                var stage, key;

                debug('update active: %s', this.scroll);

                this.active = '';

                if (this.scroll >= 28975){
                    this.active = 'downloads';
                    this.clearPane();
                }
                else if (this.scroll >= 28503) {
                    this.active = 'produktprogramm';
                    this.clearPane();
                }
                else if (this.scroll >= 27601) {
                    this.active = 'kontakt';
                    this.clearPane();
                }
                else {
                    console.info('this.scroll: %s', this.scroll);
                    this.active = '';
                    if(n) {
                        for(key in this.stages) {
                            stage = this.stages[key];
                            if(n == stage.mainFrame) {
                                this.pane = key;
                                break;
                            }
                        }
                    }
                    else if (!keepStage) {
                        for (key in this.stages) {
                            stage = this.stages[key];
                            if (Math.ceil(this.targetPosition / this.ratio) >= stage.beginFrame &&
                                Math.ceil(this.targetPosition / this.ratio) <= stage.endFrame)
                                break;
                        }

                        // Set the pane on a delay so that there is time for
                        // the change-out animation to finish
                        setTimeout(function() {
                            debug('set pane: %s', key);
                            this.pane = key;
                        }.bind(this), 400);
                    }
                }

                return stage;
            },

            fitFrame: function(n, big, dontClear) {
                if (!n && n !== 0)
                    n = this.scene.currentFrame;

                if (!dontClear)
                    this.ctx.clearRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);

                if (big)
                    fitImageOn(this.canvas, this.ctx, this.frames[n].big);
                else
                    fitImageOn(this.canvas, this.ctx, this.frames[n].small);

                this.$emit('display-frame', this.frames[n], big);
            },

            // Display a particular frame on the canvas
            showFrame: function(n) {
                // Make sure we don't go past the last frame
                if (n > this.scene.lastFrame)
                    n = this.scene.lastFrame;

                if (this.frames[n].small._isLoaded) {
                    debug('showFrame(%s) : Frame Loaded, show it', n);
                    // Clear the old frame and draw the new one
                    this.fitFrame(n);

                    // Start timer to load a high-res version of the current frame.
                    //
                    // If the frame doesn't change again before the time runs out
                    // the canvas will be render the hd version of the frame
                    clearTimeout(this.timeoutId);

                    if (n == Math.ceil(this.scroll / this.ratio)) {
                        this.timeoutId = setTimeout(function() {
                            if (this.frames[n].big._isLoaded) {
                                this.fitFrame(n, true);
                            } else {
                                debug('start loading hd frame: %s', n);
                                this.frames[n]._loadBig();

                                $(this.frames[n].big).one('load', (function(me) {
                                    return function handler() {
                                        (function fadeInBig(opacity) {
                                            opacity = opacity || 0;

                                            if (Math.ceil(me.scroll / me.ratio) == n && opacity < 100) {
                                                opacity += 5;

                                                debug('load big: %s => %s @ %s', me.scene.currentFrame, n, opacity);

                                                me.ctx.save();
                                                me.ctx.globalAlpha = Math.min(opacity,100) / 100;
                                                me.fitFrame(n, true, true);
                                                me.ctx.restore();

                                                setTimeout(function() { fadeInBig(opacity); }, 10);
                                            }
                                        })();
                                    };
                                })(this));
                            }
                        }.bind(this), 1000);
                    }

                    setTimeout(function() {
                        this.loadFrames(n);
                    }.bind(this), 0);
                } else {
                    debug('showFrame(%s) : Frame not loaded, attach handler', n);

                    $(this.frames[n].small).one('load', (function(me) {
                        return function handler() {
                            debug('showFrame(%s) : Frame finished load, show it', n);

                            // Only draw frames that are near the current frame,
                            // and even then only draw them if the current frame
                            // isn't actually up itself

                            // if (n == me.scene.currentFrame || ((
                            //       (n >= me.scene.currentFrame - 3 && n <= me.scene.currentFrame) ||
                            //       (n <= me.scene.currentFrame + 3 && n >= me.scene.currentFrame)
                            //      ) && !me.frames[me.scene.currentFrame].isLoaded)) {

                                // Clear the old frame and draw the new one
                                me.fitFrame(n);
                            // }

                            // Start timer to load a high-res version of the current frame.
                            //
                            // If the frame doesn't change again before the time runs out
                            // the canvas will be render the hd version of the frame
                            clearTimeout(me.timeoutId);
                            if (Math.ceil(me.scroll / me.ratio) == n) {
                                me.timeoutId = setTimeout(function() {
                                    if (me.frames[n].big._isLoaded) {
                                        me.fitFrame(n, true);
                                    } else {
                                        debug('start loading hd frame: %s', n);
                                        me.frames[n]._loadBig();

                                        $(me.frames[n].big).one('load', (function(me) {
                                            return function handler() {
                                                (function fadeInBig(opacity) {
                                                    opacity = opacity || 0;

                                                    if (Math.ceil(me.scroll / me.ratio) == n && opacity < 100) {
                                                        opacity += 5;

                                                        debug('load big: %s => %s @ %s', me.scene.currentFrame, n, opacity);

                                                        me.ctx.save();
                                                        me.ctx.globalAlpha = Math.min(opacity,100) / 100;
                                                        me.fitFrame(n, true, true);
                                                        me.ctx.restore();

                                                        setTimeout(function() { fadeInBig(opacity); }, 10);
                                                    }
                                                })();
                                            };
                                        })(me));
                                    }
                                }, 1000);
                            }
                        };
                    })(this));

                    if (!this.frames[n].small._isLoading) {
                        debug('showFrame(%s) : Frame not started loading, load it', n);
                        // Start loading
                        this.frames[n]._load();
                    }

                    // Don't start loading other frames right away, make sure
                    // the first one gets precedence
                    setTimeout(function() {
                        this.loadFrames(n);
                    }.bind(this), 0);
                }
            },

            // Skip directly to a particular frame without scrolling
            skipTo: function(n, anchor) {
                this.trackNavEvent(n);
                this.isSkipping = true;

                if(anchor) {
                    window.location.hash = anchor;
                }

                // If we're already here then there is nothing to do
                if (n === this.scene.currentFrame)
                    return;

                // Make sure we don't go past the last frame
                if (n > this.scene.lastFrame)
                    n = this.scene.lastFrame;

                window.cancelAnimationFrame(this.scene.rafId);
                this.scene.rafId = null;

                // Scroll directly to the position
                this.scroll = (n * this.ratio);
                this.showFrame(n, true);

                // Set the frames
                this.scene.targetFrame = n;
                this.scene.currentFrame = n;

                this.updateActivePane(false, n);
                this.$emit('animation-finished');

                this.closeAllModals();
                this.isMenuOpen = false;

            },

            scrollToFrame: function(n, dontIgnore) {
                this.scrollTo(n * this.ratio, dontIgnore);
            },

            scrollTo: function(n, dontIgnore, inertia) {
                if(this.isScrollPaused)
                    return;

                debug('.scrollTo(%s)', n);

                var self = this;

                self.targetPosition = Math.ceil(n);
                self.scene.targetFrame = Math.ceil(n * self.ratio);

                // Cancel the previous animation with the new one
                debug('cancel animation frame: %s', self.scene.rafId);
                window.cancelAnimationFrame(self.scene.rafId);

                function stepTo(fps) {
                    fps = fps || self.animation.fps;
                    // var interval = 1000 / (fps * self.ratio);
                    var interval = 1000 / fps;
                    self.animation.now = Date.now();
                    self.animation.delta = self.animation.now - self.animation.then;

                    if (self.animation.delta > interval) {

                        self.animation.then = self.animation.now - (self.animation.delta % interval);

                        //This is the problem area for WAGOMS-40
                        //The return calls before animation-finished has a chance to fire.
                        if (self.isScrollPaused) {
                            self.$emit('animation-finished', true);
                            return;
                        }

                        debug('step from %s to %s', self.scroll, self.targetPosition);

                        // Hide the auto-scroll arrow while we're scrolling
                        self.$.scrollAlias.isHidden = true;

                        if (!dontIgnore) {
                            self.ignoreScroll = true;
                        }

                        // Depending on how far away the target frame is (diff)
                        // increase the speed of the animation by skipping frames
                        var slow = 1.75;
                        var by = 1/slow; // step by n frames
                        var diff = Math.abs(self.scroll- self.targetPosition);
                        if (diff >= 200 * self.ratio)
                            by = 5/slow;
                        else if (diff >= 100 * self.ratio)
                            by = 4/slow;
                        else if (diff >= 50 * self.ratio)
                            by = 3/slow;
                        else if (diff >= 5 * self.ratio)
                            by = 2/slow;
                        else if (diff >= 25)
                            by = (self.ratio * (0.75/slow)) / self.ratio;
                        else if (diff >= 15)
                            by = (self.ratio * (0.5/slow)) / self.ratio;
                        else if (diff >= 10)
                            by = (self.ratio * (0.15/slow)) / self.ratio;
                        else if (diff >= 0)
                            by = 1 / self.ratio;

                        var isForward = self.scroll < self.targetPosition;

                        // Calculate how many frames (and which direction) to
                        // scroll by, then go
                        var step = isForward ? by : by * -1;

                        var pos = self.scroll + step * self.ratio;
                        if ((isForward && pos > self.targetPosition) ||
                           (!isForward && pos < self.targetPosition)) {
                            pos = self.targetPosition;
                        }

                        if (!dontIgnore) {
                            setTimeout(function() {
                                self.scroll = pos;
                            }, 0);
                        } else {
                            debug('FORWARD: %s', !!isForward);
                            self.bodyScrollTop = pos;
                        }

                        // setTimeout(function() {
                        //     self.scroll = pos;
                        // }, 0);

                        var frame = Math.ceil(pos / self.ratio);
                        if (frame < 0) frame = 0;

                        if(self.browserDetect.browser == 'ie') {
                            var mainFrame = self.stages[self.pane].mainFrame;
                            var controlFrame = isForward ? mainFrame + (5 * -1) : mainFrame + 5;
                            self.scrollRate = frame > controlFrame ? 450 : 900;
                        }

                        else if (frame > self.scene.lastFrame) frame = self.scene.lastFrame;
                        self.scene.currentFrame = frame;

                        if (self.scene.targetFrame >= self.scene.lastFrame)
                            self.scene.targetFrame = self.scene.lastFrame;

                        if (self.scroll != self.targetPosition)
                            setTimeout(function() {
                                self.scene.rafId = window.requestAnimationFrame(stepTo.bind(self, fps));
                                self.$emit('animation-scroll', frame);
                            }, 0);
                        else {
                            // End of the animation
                            self.$emit('animation-finished');
                        }
                    } else {
                        self.scene.rafId = window.requestAnimationFrame(stepTo.bind(self, fps));
                    }
                }

                self.scene.rafId = window.requestAnimationFrame(stepTo.bind(self, self.animation.fps));
            },

            nextFrame: function(n) {
                // stage introstop is the basic starting position.
                // If we go past the start frame close the header menu
                if (n > this.stages.introstop.mainFrame) {
                    this.isHeaderOpen = false;
                }

                debug('.nextFrame(%s)', n);
                this.loadFrames(n);

                if (this.isScrollPaused)
                    return;

                var stage = this.updateActivePane();

                // Show the intro again, if we scroll back to the first frame
                if (n == 0) this.fadeIntro();


                // If a checkpoint is scrolled to lock scrolling in place
                if (n === stage.mainFrame && !this.ignoreScroll) {
                    debug('scrolling paused at %s', n);
                    this.isScrollPaused = true;

                    // Reset scroll position to the locked frame
                    setTimeout(function() {
                        var scroll = stage.mainFrame * this.ratio;
                        this.bodyScrollTop = scroll;
                        this.scroll = scroll;
                        this.scrollTop = scroll;
                    }.bind(this), 0);

                    // After a time, release scrolling
                    setTimeout(function() {
                        this.isScrollPaused = false;
                        this.$.scrollAlias.isHidden = false;
                        debug('scrolling un-paused from %s', n);
                    }.bind(this), 2000);
                }

                this.showFrame(n);
            },

            step: function(n) {
                debug('step(%s)', n);
                var previousFrame = this.scene.currentFrame;
                this.scene.currentFrame += n;

                if (this.scene.currentFrame < 0) {
                    this.scene.currentFrame = 0;
                } else if (this.currentFrame > this.scene.lastFrame) {
                    this.scene.currentFrame = this.scene.lastFrame;
                } else {
                    n = ( this.scene.currentFrame < 0 ) ? 0 : this.scene.currentFrame;
                    this.nextFrame(this.scene.currentFrame);
                }
            },

            to: function() {
                this.closeAllModals();
                this.isMenuOpen = false;
                this.scrollToFrame.apply(this, arguments);
            },

            showContact: function() {
                this.closeAllModals();
                this.clearPane();

                window.location.hash = '#kontakt';
                window.location.href = '#kontakt';

                this.trackNavEvent(null, 'kontakt');

                this.scroll = 27601;
                this.active = 'kontakt';
                this.isMenuOpen = false;
            },

            showProductProgram: function() {
                this.closeAllModals();
                this.clearPane();

                window.location.hash = '#produktprogramm';
                window.location.href = '#produktprogramm';

                this.active = 'produktprogramm';
                this.trackNavEvent(null, 'produktprogramm');
                this.scroll = 28503;
                this.isMenuOpen = false;
            },

            showDownloads: function() {
                this.closeAllModals();
                this.clearPane();

                window.location.hash = '#downloads';
                window.location.href = '#downloads';

                this.trackNavEvent(null, 'downloads');
                this.active = 'downloads';
                this.scroll = 28975;
                this.isMenuOpen = false;
            },

            clearPane: function() {
                window.cancelAnimationFrame(this.scene.rafId);
                this.scene.rafId = null;
                this.pane = '';
            },

            alignPopouts: function() {
                // Re-position pop-out labels
                var leiter = $('.__toggle-2 .popout__label');
                var sicher = $('.__toggle-5 .popout__label');
                var besh = $('.__toggle-4 .popout__label');

                var leiterDimensions = leiter.get(0).getBoundingClientRect();
                var sicherDimensions = sicher.get(0).getBoundingClientRect();
                var beshDimensions = besh.get(0).getBoundingClientRect();
                sicher.css({ right: '-=' + (leiterDimensions.left - sicherDimensions.left) });
                besh.css({ right: '-=' + (leiterDimensions.left - beshDimensions.left) });
            },

            closeAllModals: function() {
                debug('closing all modals');

                for (var i = 0; i < window._modals.length; i++) {
                    var modal = window._modals[i];

                    if (modal && modal.close)
                        modal.close();
                }
            },

            getFrameFromHash: function(hash) {
                var mainFrame = 0;
                switch(hash) {
                    case '#cage-clamp-technology':
                        mainFrame = this.stages.clamp.mainFrame;
                        break;
                    case '#alle-leiterarten':
                        mainFrame = this.stages.universeller.mainFrame;
                        break;
                    case '#hohe-sicherheitsreserven':
                        mainFrame = this.stages.hohe.mainFrame;
                        break;
                    case '#multifunktionales-brückersystem':
                        mainFrame = this.stages.multifunktionales.mainFrame;
                        break;
                    case '#schnellste-beschriftung':
                        mainFrame = this.stages.komfortable.mainFrame;
                        break;
                    case '#alle-produktvorteile':
                        mainFrame = this.stages.moreInfo.mainFrame;
                        break;
                    default:
                        mainFrame = this.stages.introstop.mainFrame;
                        break;
                }
                return mainFrame;
            },


            // --- GOOGLE ANALYTICS TRACKING ---
            trackNavEvent: function(frame, staticPage) {
                var category = 'op_topjobs',
                    action = 'navileft',
                    checkpoint = '';

                if(!staticPage) {
                    for(var key in this.stages) {
                        if(frame == this.stages[key].mainFrame) {
                            switch(key) {
                                case 'clamp':
                                    checkpoint = 'push-in cage clamp';
                                    break;
                                case 'universeller':
                                    checkpoint = 'leiterarten';
                                    break;
                                case 'hohe':
                                    checkpoint = 'sicherheitsreserven';
                                    break;
                                case 'multifunktionales':
                                    checkpoint = 'brückerprogramm';
                                    break;
                                case 'komfortable':
                                    checkpoint = 'beschriftung';
                                    break;
                                case 'moreInfo':
                                    checkpoint = 'produktvorteile';
                                    break;
                            }
                        }
                    }
                } else {
                    checkpoint = staticPage;
                }

                this.fireTrackEvent(category, action, checkpoint);
            },

            trackMoreInfo: function(_checkpoint) {

                var category = 'op_topjobs',
                    action = 'more',
                    checkpoint = '';

                switch(_checkpoint) {
                    case 'clamp':
                        checkpoint = 'push-in cage clamp';
                        break;
                    case 'leiterarten':
                        checkpoint = _checkpoint;
                        break;
                    case 'hohe':
                        checkpoint = 'sicherheitsreserven';
                        break;
                    case 'bruckerprogramm':
                        checkpoint = 'brückerprogramm';
                        break;
                    case 'komfortable':
                        checkpoint = 'beschriftung';
                        break;
                }

                this.fireTrackEvent(category, action, checkpoint);
            },

            trackScrollNext: function(frame, _checkpoint) {
                var category = 'op_topjobs',
                    action = 'scrolldown-arrow',
                    checkpoint = '';

                if(!_checkpoint) {
                    for(var key in this.stages) {
                        if(frame == this.stages[key].mainFrame) {
                            switch(key) {
                                case 'clamp':
                                    checkpoint = 'push-in cage clamp';
                                    break;
                                case 'universeller':
                                    checkpoint = 'leiterarten';
                                    break;
                                case 'hohe':
                                    checkpoint = 'sicherheitsreserven';
                                    break;
                                case 'multifunktionales':
                                    checkpoint = 'brückerprogramm';
                                    break;
                                case 'komfortable':
                                    checkpoint = 'beschriftung';
                                    break;
                                case 'moreInfo':
                                    checkpoint = 'produktvorteile';
                                    break;
                            }
                        }
                    }
                } else {
                    checkpoint = _checkpoint;
                }

                this.fireTrackEvent(category, action, checkpoint);
            },

            fireTrackEvent: function(category, action, label) {
                _gaq.push(['_trackEvent', category, action, label]);
            }
            // -/- GOOGLE ANALYTICS TRACKING ---
        },
    });
};
