'use strict';

var debug = require('debug')('wago:vms:side-menu');

var Vue = require('vue');

module.exports = function(selector) {
    selector = selector || '.js-vm-side-menu';

    return new Vue({
        el: selector,

        data: {
            isHidden: false,
            active: null,
        },

        created: function() {
            debug('created');
        },
    });
};
