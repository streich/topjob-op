'use strict';
/* jslint ignore:start */

module.exports = (function() {

    function getGeometry() {
        var obj = {};
        // obj.w = window.innerWidth || document.body.offsetWidth;
        // obj.h = window.innerHeight || document.body.offsetHeight;
        obj.w =  document.body.offsetWidth;
        obj.h =  document.body.offsetHeight;
        obj.cx = obj.w/2;
        obj.cy = obj.h/2;
        return obj;
    }

    function filterInt(value) {
        if(/^(\-|\+)?([0-9]+|Infinity)$/.test(value))
            return Number(value);

        return NaN;
    }


    function getModalMarginSize(value) {
        var marginPercent = 18;

        if(value) {
            var intModalSize = this.filterInt(value);

            if(!isNaN(intModalSize) && intModalSize > 0 && intModalSize !== Infinity)
                marginPercent = ((100 - value) / 2);
        }

        return { marginLeft: marginPercent + '%', marginRight: marginPercent + '%' };
    }

    return {
        getGeometry: getGeometry,
        filterInt: filterInt,
        getModalMarginSize: getModalMarginSize
    };

}) ();

