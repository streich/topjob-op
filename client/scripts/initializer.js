'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:initializer');
var getQueryValue = require('../../lib/getQueryValue');
var $ = require('jquery');
window.getQueryValue = getQueryValue;

var extend = require('../../lib/extend'),
    Emitter = require('../../lib/emitter');

// var scenes = require('./scenes.js');
// var utils = require('./utils.js');

module.exports = (function() { // IIFE

    function Initializer() {
        Emitter.call(this);

        this.ctx = null;
        this.progress = null;
        this.directory = 'client/images/frames/';
        this.smallDirectory = this.directory + 'small/';
        this.beginIndex = 0;
        this.endIndex = 361;
        this.frameToRunAnimation = 0;
        this.sources = [];
        this.frames = [];
        this.templateUrl = 'frame_0';
    }

    extend(Initializer, Emitter);

    Initializer.prototype.initialize = function(minimumAnimFrame) {
        var ctx = this.ctx;

        this.progress = new LoadProgress(ctx);
        this.frameToRunAnimation = minimumAnimFrame;
        var url = '',
            urlSmall = '',
            num = 0,
            uploaded = 0;

        for(var i = this.beginIndex; i <= this.endIndex;  i++) {
            url =
                this.directory +
                this.templateUrl + ( ( i < 100 ) ? (( i < 10 ) ? '00' : '0') : '') + i + '.jpg';
            urlSmall =
                this.smallDirectory +
                this.templateUrl + ( ( i < 100 ) ? (( i < 10 ) ? '00' : '0') : '') + i + '.jpg';
            this.sources.push({
                big: url,
                small: getQueryValue('hd') ? url : urlSmall
            });
        }

        num = this.sources.length;

        var iId = setInterval(function () {
            var x = 1 - ((this.frameToRunAnimation - uploaded)/this.frameToRunAnimation);
            this.progress.draw(x);
        }.bind(this), 200);

        /**
         * Return a handler to be ran every time a frame loads
         */
        function onFrameLoad(frame,index) {
            return function() {
                debug('loaded frame: %s', index, frame);
                frame._isLoaded = true;
                frame._isLoading = false;
            };
        }

        /**
         * Return a handler to be ran the first time a frame loads
         */
        function oneFrameLoad(frame, index, me) {
            return function() {
                num--;
                uploaded++;

                if (uploaded == me.frameToRunAnimation){
                    me.ctx.clearRect(0, 0, me.ctx.canvas.width, me.ctx.canvas.height);
                    me.$emit('initial-frames-ready');
                    clearInterval(iId);
                }
            };
        }

        /**
         * Return a function that can be used to load a frame
         */
        function loadSmallFrame(frame, src, index) {
            return function() {
                frame.small._isLoading = true;
                frame.small.src = src.small;
                debug('loading frame-sm: %s', index, frame.small);
            };
        }

        /**
         * Return a function that can be used to load a frame
         */
        function loadBigFrame(frame, src, index) {
            return function() {
                frame.big._isLoading = true;
                frame.big.src = src.big;
                debug('loading frame-hd: %s', index, frame.big);
            };
        }

        for(var j = 0; j < this.sources.length; j++) {
            this.frames[j] = {
                small: new Image(),
                big: new Image(),
            };

            this.frames[j].small.addEventListener('load', onFrameLoad(this.frames[j].small, j, this));
            this.frames[j].big.addEventListener('load', onFrameLoad(this.frames[j].big, j, this));

            $(this.frames[j].small).one('load', oneFrameLoad(this.frames[j].small, j, this));

            this.frames[j]._load = loadSmallFrame(this.frames[j],this.sources[j],j);
            this.frames[j]._loadBig = loadBigFrame(this.frames[j],this.sources[j],j);

            // Load frames to the first checkpoint
            if (j <= this.frameToRunAnimation) {
                debug('initialize frame: %s (%s)', j, this.frameToRunAnimation);
                this.frames[j]._load();
            }
        }

        this.$emit('images-initialized', this.frames);
    };


    function LoadProgress(ctx) {
        var r = Math.round(ctx.canvas.height/24),
            fs = Math.round(r/1.2),
            cx = ctx.canvas.width/2,
            cy = ctx.canvas.height/2,
            color = '#AAA',
            font = fs + 'px futura',
            startAngle = Math.PI * 1.5;

        this.draw = function (x) {
            var finAngle = startAngle + x * Math.PI * 2,
                pr = x * 100,
                text = pr.toFixed(0) + '%';
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            ctx.save();
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.lineWidth = '3';
            ctx.strokeStyle = color;
            ctx.fillStyle = color;
            ctx.font = font;
            ctx.beginPath();
            ctx.arc(cx, cy, r, startAngle, finAngle, false);
            ctx.stroke();
            ctx.fillText(text, cx, cy);
            ctx.restore();
        }.bind(this);
    }

    return new Initializer();

})();
