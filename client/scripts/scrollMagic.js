'use strict';
/* jslint browser:true */

/**
 * Setup and return ScrollMagic
 */

var ScrollMagic = window.ScrollMagic = require('scrollmagic');
window.TweenMax = require('gsap');
window.TimelineMax = require('gsap/src/uncompressed/TimelineMax');

require('scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap');
require('scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators');

module.exports = ScrollMagic;
