'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-multifunktionales');

var Vue = require('vue');

module.exports = Vue.component('pane-multifunktionales', {
    template: '#vue-tmpl__pane-multifunktionales',

    data: function() {
        return {};
    },

    created: function() {
        debug('created');
    },
});
