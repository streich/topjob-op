'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-komfortable');

var Vue = require('vue');

module.exports = Vue.component('pane-komfortable', {
    template: '#vue-tmpl__pane-komfortable',

    data: function() {
        return {};
    },

    created: function() {
        debug('created');
    },
});
