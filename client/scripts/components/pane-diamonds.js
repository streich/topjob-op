'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-diamonds');

var Vue = require('vue');

module.exports = Vue.component('pane-diamonds', {
    template: '#vue-tmpl__pane-diamonds',

    data: function() {
        return {};
    },

    created: function() {
        debug('created');
    },

    methods: {
        go: function(url) {
            window.open(url,'_blank');
        }
    }
});
