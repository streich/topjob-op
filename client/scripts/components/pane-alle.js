'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-alle');

var Vue = require('vue');

module.exports = Vue.component('pane-alle', {
    template: '#vue-tmpl__pane-alle',

    data: function() {
        return {};
    },

    created: function() {
        debug('created');
    },
});
