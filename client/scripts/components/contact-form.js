'use strict';
/* jslint browser:true */
/* global _gaq */

var debug = require('debug')('wage:cmpt:contact-form');

var Vue = require('vue'),
    request = require('superagent');

module.exports = Vue.component('contact-form', {
    template: '#vue-tmpl__contact-form',

    data: function() {
        return {
            isSending: false,
            isSent: false,
            message: {
                name:   '',
                city:   '',
                stadt:  '',
                plz:    '',
                email:  '',
                firmenname: '',
                kundennummer: '',
                anmerkungen: '',
                informationsMaterial: false,
                consultation: false,
                personalConsultation: false,
                confirm: false,
            }
        };
    },

    created: function() {
        debug('created');
        window.vms.contact = this;
    },

    methods: {
        reset: function() {
            this.isSending = false;
            this.isSent = false;

            this.message.name = '';
            this.message.city = '';
            this.message.stadt = '';
            this.message.plz = '';
            this.message.email = '';

            this.message.firmenname = '';
            this.message.kundennummer = '';
            this.message.anmerkungen = '';

            this.message.confirm = false;
            this.message.informationsMaterial = false;
            this.message.consultation = false;
            this.message.personalConsultation = false;
        },

        send: function(isMuster, e) {

              var failed = false;
              $.each(  $('.contact-item__form [required]'),function(){
                  if($(this).val()==''){
                  failed = true;
                  }
                });
                if(failed){
                  console.log("failed",failed);
                  e.preventDefault();
                  return false;
                }


            if (e && e.preventDefault) e.preventDefault();
            if (e && e.stopPropagation) e.stopPropagation();
            if (this.isSending) return;

            var category = 'op_topjobs',
                action = '',
                event = 'send';

            if(isMuster)
                action = 'musterbestellung';
            else
                action = 'kontaktform';

            _gaq.push(['_trackEvent', category, action, event]);

            this.isSending = true;
            request.post('/contact.php')
                   .send(this.message)
                   .end(function(err, res) {
                       debug('err: ', err);
                       debug('res: ', res);

                       this.isSending = false;
                       this.isSent = true;

                       setTimeout(this.reset, 2500);
                   }.bind(this));

            return false;
        },
    },
});
