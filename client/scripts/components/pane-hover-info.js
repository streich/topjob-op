'use strict';
/* jslint browser:true */

var debug = require('debug')('wage:cmpt:hover-info');

var Vue = require('vue');

module.exports = Vue.component('pane-hover-info', {
    template: '#vue-tmpl__pane-hover-info',

    data: function() {
    },

    created: function() {
        debug('created');
    },

    methods: {
    },
});