'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-moreinfo');

require('./pop-out');

var $ = require('jquery');
var Vue = require('vue');

module.exports = Vue.component('pane-moreinfo', {
    template: '#vue-tmpl__pane-moreInfo',

    data: function() {
        return {};
    },

    ready: function() {
        var self = this;

        if (self.$parent.isStatic) {
            setTimeout(function() {
                $('.popout__button').click(function(e) {
                    e.preventDefault();
                    e.stopPropagation();

                    if ($(this).parents('.__toggle-1').length)
                        self.$parent.toggleModal('clamp');
                    else if ($(this).parents('.__toggle-2').length)
                        self.$parent.toggleModal('leiterarten');
                    else if ($(this).parents('.__toggle-3').length)
                        self.$parent.toggleModal('bruckerprogramm');
                    else if ($(this).parents('.__toggle-4').length)
                        self.$parent.toggleModal('komfortable');
                    else if ($(this).parents('.__toggle-5').length)
                        self.$parent.toggleModal('hohe');

                    return false;
                });
            }, 200);
        }
    },

    created: function() {
        debug('created');
    },
});
