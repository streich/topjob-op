'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-hohe');

var Vue = require('vue');

module.exports = Vue.component('pane-hohe', {
    template: '#vue-tmpl__pane-hohe',

    data: function() {
        return {};
    },

    created: function() {
        debug('created');
    },
});
