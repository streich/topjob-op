'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-intro-stop');

var Vue = require('vue');

module.exports = Vue.component('pane-intro-stop', {
    template: '#vue-tmpl__pane-intro-stop',

    data: function() {
        return {};
    },

    created: function() {
        debug('created');
    },
});
