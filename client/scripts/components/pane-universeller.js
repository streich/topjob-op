'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-universeller');

var Vue = require('vue');

module.exports = Vue.component('pane-universeller', {
    template: '#vue-tmpl__pane-universeller',

    data: function() {
        return {};
    },

    created: function() {
        debug('created');
    },
});
