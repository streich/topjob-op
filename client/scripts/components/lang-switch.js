'use strict';

var debug = require('debug')('wago:cmpt:lang-switch');

var Vue = require('vue');
var Lang = require('../language');

module.exports = Vue.component('lang-switch', {
    template: '#vue-tmpl__lang-switch',

    data: function() {
        return {
          isLangSwitchOpen: false,
          langID: 'de',
        };
    },

    created: function() {
      var query = function () {
        // This function is anonymous, is executed immediately and
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split('&');
        for (var i=0;i<vars.length;i++) {
          var pair = vars[i].split('=');
              // If first entry with this name
          if (typeof query_string[pair[0]] === 'undefined') {
            query_string[pair[0]] = pair[1];
              // If second entry with this name
          } else if (typeof query_string[pair[0]] === 'string') {
            var arr = [ query_string[pair[0]], pair[1] ];
            query_string[pair[0]] = arr;
              // If third or later entry with this name
          } else {
            query_string[pair[0]].push(pair[1]);
          }
        }
          return query_string;
      } ();

      var langID = query.lang;
      if(!langID){
          if(localStorage && localStorage.getItem('wagoTopjobLang')){
            langID = localStorage.getItem('wagoTopjobLang');
          }else{
            langID = 'de';
          }
      }
      var _this = this;
        debug('created');
        this.setLang(langID);
    },

    methods: {

      setLang: function(lang){
        this.isLangSwitchOpen = false;
        this.langID = lang;
        this.langIDClass = 'flag-icon-'+lang;
        $('.lang-toggle div').attr('class','flag-icon flag-icon-'+lang);
        var lang = new Lang('client/language/'+lang+'.xml');
        if(localStorage) localStorage.setItem('wagoTopjobLang',this.langID);

        lang.load(function(keys) {
            this.lang = keys;
            this.isLoaded = true;
        }.bind(this.$parent));
      }
    }
});
