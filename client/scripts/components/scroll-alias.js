'use strict';
/* jslint browser:true */

var debug = require('debug')('wage:cmpt:scroll-alias');

var Vue = require('vue');

module.exports = Vue.component('scroll-alias', {
    template: '#vue-tmpl__scroll-alias',

    data: function() {
        return {
            isHidden: true,
            isHovering: false,

            isTextHidden: false,

            isArrowDown: false,
            isRopeShowing: false,
        };
    },

    created: function() {
        debug('created');
    },
});
