'use strict';

var debug = require('debug')('wago:cmpt:side-menu');

var Vue = require('vue');

module.exports = Vue.component('side-menu', {
    template: '#vue-tmpl__side-menu',

    data: function() {
        return {
            isHidden: true,
            isOpen: true,
            active: null,
        };
    },

    created: function() {
      var _this = this;
        debug('created');

      
    },

    methods: {
        setPane: function(pane, hash) {
            if (this.$parent.isStatic)
                this.$parent.setPane(pane);
            else
                this.$parent.skipTo(this.$parent.stages[pane].mainFrame, hash);
        },
    }

});
