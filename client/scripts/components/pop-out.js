'use strict';
/* jslint browser:true */
/* global _gaq */

var debug = require('debug')('wage:cmpt:pop-out');

var Vue = require('vue');
var $ = require('jquery');

module.exports = Vue.component('pop-out', {
    template: '#vue-tmpl__pop-out',

    data: function() {
        return {
            isOpen: false,
        };
    },

    created: function() {
        debug('created');

        if(!window._popouts)
            window._popouts = [];

        window._popouts.push(this);
    },

    methods: {

        toggle: function(name, e) {
            if (this.$parent &&
               (this.$parent.isStatic || (
                this.$parent.$parent &&
                this.$parent.$parent.isStatic)))
                return;

            if (e && e.preventDefault) e.preventDefault();
            if(this.isOpen)
                this.close();
            else {
                this.trackEvent(name);
                this.closeAll();
                this.open();
            }

            return false;
        },

        open: function(e) {
            if (e && e.preventDefault) e.preventDefault();
            this.isOpen = true;
            this.hideLabels();

            return false;
        },

        close: function(e) {
            if (e && e.preventDefault) e.preventDefault();

            this.isOpen = false;
            this.showLabels();

            return false;
        },

        closeAll: function(e) {
            if(e && e.preventDefault) e.preventDefault();

            for(var i = 0; i < window._popouts.length; i++) {
                var popout = window._popouts[i];

                if(popout && popout.close)
                    popout.close();
            }
        },

        hideLabels: function(e) {
            if(e && e.preventDefault) e.preventDefault();

            $('.popout__label').addClass('hidden');
        },

        showLabels: function(e) {
            if(e && e.preventDefault) e.preventDefault();

            $('.popout__label').removeClass('hidden');
        },

        trackEvent: function(name) {
            _gaq.push(['_trackEvent', 'op_topjobs', 'produktvorteile', name]);
        }
    }
});
