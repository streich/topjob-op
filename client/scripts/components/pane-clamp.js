'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-clamp');

var Vue = require('vue');

module.exports = Vue.component('pane-clamp', {
    template: '#vue-tmpl__pane-clamp',

    data: function() {
        return {};
    },

    created: function() {
        debug('created');
    },
});
