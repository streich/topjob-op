'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-intro');

var Vue = require('vue');

module.exports = Vue.component('pane-intro', {
    template: '#vue-tmpl__pane-intro',

    data:  function() { 
        return { };
    },

    created: function() {
        debug('created');
        

    },
});
