'use strict';
/* jslint browser:true */

var debug = require('debug')('wage:cmpt:info-modal');

var Vue = require('vue');


module.exports = Vue.component('info-modal', {
    template: '#vue-tmpl__info-modal',

    data: function() {
        return {
            isOpen: false,
        };
    },

    created: function() {
        debug('created');

        if (!window._modals)
            window._modals = [];

        window._modals.push(this);
    },

    methods: {

        toggle: function(e) {
            if (e && e.preventDefault) e.preventDefault();

            if(this.isOpen)
                this.close();
            else
                this.open();

            return false;
        },

        open: function(e) {
            if (e && e.preventDefault) e.preventDefault();

            if(!this.isOpen) {
                this.$parent.isIgnoringUserScroll = true;

                if (this.$parent.$.scrollAlias)
                    this.$parent.$.scrollAlias.isHidden = true;

                this.isOpen = true;
            }
            return false;
        },

        close: function(e) {
            if (e && e.preventDefault) e.preventDefault();

            if(this.isOpen) {
                this.$parent.isIgnoringUserScroll = false;

                if (this.$parent.$.scrollAlias)
                    this.$parent.$.scrollAlias.isHidden = false;

                this.isOpen = false;
            }

            return false;
        },
    }
});

