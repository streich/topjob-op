'use strict';
/* jslint browser:true */

var debug = require('debug')('wago:cmpt:pane-contact');

var Vue = require('vue');

module.exports = Vue.component('pane-contact', {
    template: '#vue-tmpl__pane-contact',

    data: function() {
        return {
            isMuster: false,
            isContactFormVisible: true,
        };
    },

    created: function() {
        debug('created');
    },
});

