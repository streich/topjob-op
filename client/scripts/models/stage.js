'use strict';

var extend = require('../../../lib/extend'),
    Emitter = require('../../../lib/emitter');

var Stage = function(positions, isVisible) {
    Emitter.call(this);

    this.isVisible = !!isVisible;
    this.positions = positions;
};

extend(Stage, Emitter);

module.exports = Stage;
