'use strict';

var extend = require('../../../lib/extend'),
    Emitter = require('../../../lib/emitter');

var AppEvents = function() {
    Emitter.call(this);
};

extend(AppEvents, Emitter);

module.exports = AppEvents;
