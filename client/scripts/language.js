'use strict';
var request = require('superagent');
var $ = require('jquery');

/**
 * Language
 *
 * Load and return language keys
 */
function Lang(url) {
    this.keys = {};
    this.url = url;
    this.isLoaded = false;
}

/**
 * Load the language keys from a url
 */
Lang.prototype.load = function(cb, url) {
    url = url || this.url;

    request.get(url).end(function(err, res) {
        var xmlDoc = $.parseHTML(res.text),
            $xml = $(xmlDoc),
            $intro = $xml.find('intro'),
            $introStop = $xml.find('introStop'),
            $clamp = $xml.find('cageClamp'),
            $leiterarten = $xml.find('leiterarten'),
            $hohe = $xml.find('hoheSicherheitsreserven'),
            $bruckerprogramm = $xml.find('bruckerprogrammPane'),
            $komfy = $xml.find('komfortableBeschriftung'),
            $alle = $xml.find('alleProduktvorteile'),
            $header = $xml.find('header'),
            $statics = $xml.find('static'),
            $sideMenu = $xml.find('sideMenu'),
            $footer = $xml.find('footer'),
            $scrollAlias = $xml.find('scrollAlias'),
            $produktProgramm = $xml.find('produktProgramm'),
            $lang = $xml.find('lang'),
            $contact = $xml.find('contactForm');

        this.keys.lang = {};
        this.keys.lang.headerSwitch = $lang.find('switch').html();
        this.keys.lang.current = $lang.find('current').text();

        this.keys.scrollAlias = {};
        this.keys.scrollAlias.text = $scrollAlias.text();
        this.keys.header = {};
        this.keys.header.contact = $header.find('contact').text();
        this.keys.header.corporateWebsite = $header.find('corporateWebsite').text();

        this.keys.intro = {};
        this.keys.intro.lineOne = $intro.find('lineOne').text();
        this.keys.intro.lineTwo = $intro.find('lineTwo').text();
        this.keys.intro.lineThree = $intro.find('lineThree').text();
        this.keys.intro.lineFour = $intro.find('lineFour').text();

        this.keys.introstop = {};
        this.keys.introstop.logoText    = $introStop.find('logoText').html();
        this.keys.introstop.aside = {};
        this.keys.introstop.aside.title = $introStop.find('aside').find('title').text();
        this.keys.introstop.aside.text  = $introStop.find('aside').find('text').text();

        this.keys.clamp = {};
        this.keys.clamp.title   = $clamp.find('title').text();
        this.keys.clamp.buttonText = $clamp.find('buttonText').text();
        this.keys.clamp.heading = $clamp.find('heading').html();
        this.keys.clamp.content = $clamp.find('content').html();
        this.keys.clamp.modal = {};
        this.keys.clamp.modal.key = 'clamp';
        var $clamp_modal = $clamp.find('modal');
        this.keys.clamp.modal.title = $clamp_modal.find('m_title').html();
        this.keys.clamp.modal.subText = $clamp_modal.find('subText').html();
        this.keys.clamp.modal.body = $clamp_modal.find('m_body').html();

        this.keys.leiterarten = {};
        this.keys.leiterarten.title   = $leiterarten.find('title').text();
        this.keys.leiterarten.heading = $leiterarten.find('heading').html();
        this.keys.leiterarten.content = $leiterarten.find('content').html();
        this.keys.leiterarten.buttonText = $leiterarten.find('buttonText').text();
        this.keys.leiterarten.modal = {};
        this.keys.leiterarten.modal.key = 'leiterarten';
        var $leiterarten_modal = $leiterarten.find('modal');
        this.keys.leiterarten.modal.title = $leiterarten_modal.find('m_title').html();
        this.keys.leiterarten.modal.subText = $leiterarten_modal.find('subText').html();
        this.keys.leiterarten.modal.body = $leiterarten_modal.find('m_body').html();
        this.keys.leiterarten.fadeIns = {};
        var $leiterarten_fadeIns = $leiterarten.find('fadeIns');
        this.keys.leiterarten.fadeIns.leiterartenTitle = $leiterarten_fadeIns.find('leiterartenTitle').text();
        this.keys.leiterarten.fadeIns.eindrahtige = $leiterarten_fadeIns.find('eindrahtige').text();
        this.keys.leiterarten.fadeIns.feindrahtige = $leiterarten_fadeIns.find('feindrahtige').text();
        this.keys.leiterarten.fadeIns.mehdrahtige = $leiterarten_fadeIns.find('mehdrahtige').text();
        this.keys.leiterarten.fadeIns.aderendhulse = $leiterarten_fadeIns.find('aderendhulse').text();

        this.keys.hohe = {};
        this.keys.hohe.title   = $hohe.find('title').text();
        this.keys.hohe.heading = $hohe.find('heading').html();
        this.keys.hohe.content = $hohe.find('content').html();
        this.keys.hohe.buttonText = $hohe.find('buttonText').text();
        this.keys.hohe.modal = {};
        this.keys.hohe.modal.key = 'hohe';
        var $hohe_modal = $hohe.find('modal');
        this.keys.hohe.modal.title = $hohe_modal.find('m_title').html();
        this.keys.hohe.modal.subText = $hohe_modal.find('subText').html();
        this.keys.hohe.modal.body = $hohe_modal.find('m_body').html();

        this.keys.bruckerprogramm = {};
        this.keys.bruckerprogramm.title   = $bruckerprogramm.find('title').text();
        this.keys.bruckerprogramm.heading = $bruckerprogramm.find('heading').html();
        this.keys.bruckerprogramm.content = $bruckerprogramm.find('content').html();
        this.keys.bruckerprogramm.buttonText = $bruckerprogramm.find('buttonText').text();
        this.keys.bruckerprogramm.modal = {};
        this.keys.bruckerprogramm.modal.key = 'bruckerprogramm';
        var $bruckerprogramm_modal = $bruckerprogramm.find('modal');
        this.keys.bruckerprogramm.modal.title = $bruckerprogramm_modal.find('m_title').html();
        this.keys.bruckerprogramm.modal.subText = $bruckerprogramm_modal.find('subText').html();
        this.keys.bruckerprogramm.modal.body = $bruckerprogramm_modal.find('m_body').html();

        this.keys.komfortable = {};
        this.keys.komfortable.title   = $komfy.find('title').text();
        this.keys.komfortable.heading = $komfy.find('heading').html();
        this.keys.komfortable.content = $komfy.find('content').html();
        this.keys.komfortable.buttonText = $komfy.find('buttonText').text();
        this.keys.komfortable.modal = {};
        this.keys.komfortable.modal.key = 'komfortable';
        var $komfy_modal = $komfy.find('modal');
        this.keys.komfortable.modal.title = $komfy_modal.find('m_title').html();
        this.keys.komfortable.modal.subText = $komfy_modal.find('subText').html();
        this.keys.komfortable.modal.body = $komfy_modal.find('m_body').html();

        this.keys.produktprogramm = {};
        this.keys.produktprogramm.title = $produktProgramm.find('title').html();
        this.keys.produktprogramm.durch = $produktProgramm.find('durch').text();
        this.keys.produktprogramm.funk = $produktProgramm.find('funk').text();
        this.keys.produktprogramm.install = $produktProgramm.find('install').text();
        this.keys.produktprogramm.verteiler = $produktProgramm.find('verteiler').text();

        this.keys.alle = {};
        this.keys.alle.title   = $alle.find('title').text();
        this.keys.alle.heading = $alle.find('heading').html();
        this.keys.alle.content = $alle.find('content').html();
        this.keys.alle.toggle1 = $alle.find('toggle1').html();
        this.keys.alle.toggle2 = $alle.find('toggle2').html();
        this.keys.alle.toggle3 = $alle.find('toggle3').html();
        this.keys.alle.toggle4 = $alle.find('toggle4').html();
        this.keys.alle.toggle5 = $alle.find('toggle5').html();
        this.keys.alle.clamp = $alle.find('clamp').html();
        this.keys.alle.leiterarten = $alle.find('leiterarten').html();
        this.keys.alle.bruckerprogramm = $alle.find('bruckerprogramm').html();
        this.keys.alle.hohe = $alle.find('hohe').html();
        this.keys.alle.beschriftung = $alle.find('beschriftung').html();

        this.keys.contact = {};
        this.keys.contact.successMessage = $contact.find('successMessage').html();
        this.keys.contact.phoneTitle = $contact.find('phoneTitle').html();
        this.keys.contact.phoneText = $contact.find('phoneText').html();
        this.keys.contact.phoneNumber = $contact.find('phoneNumber').html();
        this.keys.contact.introLineOne = $contact.find('introLineOne').html();
        this.keys.contact.introLineTwo = $contact.find('introLineTwo').html();
        this.keys.contact.buttonContact = $contact.find('buttonContact').html();
        this.keys.contact.buttonSample = $contact.find('buttonSample').html();


        this.keys.contact.fields = {};
        this.keys.contact.fields.companyName = $contact.find('fields').find('companyName').text();
        this.keys.contact.fields.customerNumber = $contact.find('fields').find('customerNumber').text();
        this.keys.contact.fields.name = $contact.find('fields').find('name').text();
        this.keys.contact.fields.street = $contact.find('fields').find('street').text();
        this.keys.contact.fields.city = $contact.find('fields').find('city').text();
        this.keys.contact.fields.zip = $contact.find('fields').find('zip').text();
        this.keys.contact.fields.email = $contact.find('fields').find('email').text();
        this.keys.contact.fields.comments = $contact.find('fields').find('comments').text();
        this.keys.contact.fields.informationByMail = $contact.find('fields').find('informationByMail').text();
        this.keys.contact.fields.consultCall = $contact.find('fields').find('consultCall').text();
        this.keys.contact.fields.personalConsult = $contact.find('fields').find('personalConsult').text();
        this.keys.contact.fields.required = $contact.find('fields').find('required').text();
        this.keys.contact.fields.sendPattern = $contact.find('fields').find('sendPattern').text();
        this.keys.contact.fields.send = $contact.find('fields').find('send').text();

        this.keys.downloads = {};
        this.keys.downloads.all = [];
        var self = this;
        $xml.find('download').each(function() {
            var download = {
                title: $(this).find('title').text(),
                text: $(this).find('text').html(),
                link: $(this).find('downloadLink').text(),
                type: $(this).find('type').text(),
                fileSize: $(this).find('fileSize').text(),
                thumbnail: $(this).find('thumbnail').text()
            };

            self.keys.downloads.all.push(download);
        });

        this.keys.sideMenu = {};
        this.keys.sideMenu.clamp = {};
        this.keys.sideMenu.clamp.text = $sideMenu.find('clamp').find('text').html();
        this.keys.sideMenu.clamp.title = $sideMenu.find('clamp').find('title').text();
        this.keys.sideMenu.leiterarten = {};
        this.keys.sideMenu.leiterarten.text = $sideMenu.find('leiterarten').find('text').html();
        this.keys.sideMenu.leiterarten.title = $sideMenu.find('leiterarten').find('title').text();
        this.keys.sideMenu.hohe = {};
        this.keys.sideMenu.hohe.text = $sideMenu.find('hohe').find('text').html();
        this.keys.sideMenu.hohe.title = $sideMenu.find('hohe').find('title').text();
        this.keys.sideMenu.bruckerprogramm = {};
        this.keys.sideMenu.bruckerprogramm.text = $sideMenu.find('bruckerprogramm').find('text').html();
        this.keys.sideMenu.bruckerprogramm.title = $sideMenu.find('bruckerprogramm').find('title').text();
        this.keys.sideMenu.schnelle = {};
        this.keys.sideMenu.schnelle.text = $sideMenu.find('schnelle').find('text').html();
        this.keys.sideMenu.schnelle.title = $sideMenu.find('schnelle').find('title').text();
        this.keys.sideMenu.produktvorteile = {};
        this.keys.sideMenu.produktvorteile.text = $sideMenu.find('produktvorteile').find('text').html();
        this.keys.sideMenu.produktvorteile.title = $sideMenu.find('produktvorteile').find('title').text();
        this.keys.sideMenu.contact = {};
        this.keys.sideMenu.contact.text = $sideMenu.find('contact').find('text').html();
        this.keys.sideMenu.contact.title = $sideMenu.find('contact').find('title').text();
        this.keys.sideMenu.products = {};
        this.keys.sideMenu.products.text = $sideMenu.find('products').find('text').html();
        this.keys.sideMenu.products.title = $sideMenu.find('products').find('title').text();
        this.keys.sideMenu.downloads = {};
        this.keys.sideMenu.downloads.text = $sideMenu.find('downloads').find('text').html();
        this.keys.sideMenu.downloads.title = $sideMenu.find('downloads').find('title').text();

        this.keys.static = {};
        this.keys.static.imageAnchors = {};
        var $imageAnchors = $statics.find('imageAnchors');
        this.keys.static.imageAnchors.clamp = $imageAnchors.find('clamp').html();
        this.keys.static.imageAnchors.leiterarten = $imageAnchors.find('leiterarten').html();
        this.keys.static.imageAnchors.hohe = $imageAnchors.find('hohe').html();
        this.keys.static.imageAnchors.bruckerprogramm = $imageAnchors.find('bruckerprogramm').text();
        this.keys.static.imageAnchors.schnelle = $imageAnchors.find('schnelle').text();

        this.keys.footer = {};
        this.keys.footer.newsletterTitle = $footer.find('newsletterTitle').text();
        this.keys.footer.newsletterText = $footer.find('newsletterText').text();
        this.keys.footer.newsletterButton = $footer.find('newsletterButton').text();
        this.keys.footer.copyrightText = $footer.find('copyrightText').html();
        this.keys.footer.links = {};
        var $footerLinks = $footer.find('links');
        this.keys.footer.links.contact = $footerLinks.find('contact').text();
        this.keys.footer.links.impressum = $footerLinks.find('impressum').text();
        this.keys.footer.links.legalNotice = $footerLinks.find('legalNotice').text();



        this.keys.modal = {};

        this.isLoaded = true;

        // Once everything is loaded run cb, passing it the loaded keys
        cb(this.keys);

    }.bind(this));
};


module.exports = Lang;
