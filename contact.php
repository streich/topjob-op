<?php

$post = json_decode($HTTP_RAW_POST_DATA);
$res = array();

$to =   'info@wago.com';
$subject = 'TOPJOB-S Website Kontakt';

// Sanitize input
$name           = htmlspecialchars($post->name);
$city           = htmlspecialchars($post->city);
$stadt          = htmlspecialchars($post->stadt);
$plz            = htmlspecialchars($post->plz);
$email          = htmlspecialchars($post->email);
$firmenname     = htmlspecialchars($post->firmenname);
$kundennummer   = htmlspecialchars($post->kundennummer);
$anmerkungen    = htmlspecialchars($post->anmerkungen);
$confirm        = htmlspecialchars($post->confirm);
$information    = htmlspecialchars($post->informationsMaterial);
$consultation   = htmlspecialchars($post->consultation);
$personalConsult = htmlspecialchars($post->personalConsultation);

// Make super-extra sure that the email exists
if (empty($email) || !$email || strlen($email) == 0) {
    $res['success'] = false;
    echo json_encode((object)$res);
    exit();
}

// Create the message
$message = array();
$message[] = 'Contact Form';
$message[] = '';
$message[] = '';
$message[] = "Submitted By: $name <$email>";
$message[] = '';
$message[] = "City: $city";
$message[] = '';
$message[] = "Stadt: $stadt";
$message[] = '';
$message[] = "PLZ: $plz";
$message[] = '';
$message[] = "Bitte: $confirm";
$message[] = '';
$message[] = "Firmenname: $firmenname";
$message[] = '';
$message[] = "Kundennummer: $kundennummer";
$message[] = '';
$message[] = "Anmerkungen: $anmerkungen";
$message[] = '';
$message[] = 'Gewählte Optionen:';
$message[] = "Ich benötige noch mehr Informationsmaterial (postalisch): $information";
$message[] = '';
$message[] = "Ich möchte für eine unverbindliche Beratung zurück gerufen werden: $consultation";
$message[] = '';
$message[] = "Ich habe Interesse an einem persönlichen Beratungsgespräch: $personalConsult";
$message = implode("\r\n", $message);

// Set headers
$headers = array();
$headers[] = "MIME-Version: 1.0";
$headers[] = "Content-type: text/plain; charset=iso-8859-1";
$headers[] = "From: $name <$email>";
// $headers[] = "Bcc: ";
// $headers[] = "Reply-To: $name <$email>";
$headers[] = "Subject: {$subject}";
$headers[] = "X-Mailer: PHP/".phpversion();
$headers = implode("\r\n", $headers);

mail($to, $subject, $message, $headers);

$res['success'] = true;
echo json_encode((object)$res);
