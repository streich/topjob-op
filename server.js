// require('newrelic');

var debug = require('debug')('wago');

// Module Dependencies
var express = require('express');

// Load configurations
var env = process.env.NODE_ENV = process.env.NODE_ENV || 'dev',
    config = require('./config/config');

var app = express();

// Configuration
require('./config/express')(app);

// Start the application
var port = config.port;
app.listen(port);
debug('Express application started on port ' + port);
debug('Environment: ' + env);
// console.log('Express application started on port ' + port);
// console.log('Environment: ' + env);

// Show yourself
exports = module.exports = app;
