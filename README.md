# WAGO Micro-Site

## Quick Start

Clone the repo, and run `npm install`.

If you don't already have [compass](http://compass-style.org/install/) installed:

Install compass
```
$ gem update --system
$ gem install compass
```

If you don't already have [gulp](http://gulpjs.com) installed:
```
npm install -g gulp
```

Then, to run:
```
gulp
```

Parallax Version: http://localhost:4000
Video Version: http://localhost:4000/static



## Deployment

When you're ready to deploy your changes, you have to build the static version
of the index page, so that it can be uploaded via ftp.

While the node development server is running (`gulp` or `gulp app`):
```
gulp build-static
```

This will override the `index.php` and `index.html` pages in the root directory
of the project with a new version that can be pushed up.
