(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
/* global console */

function CheckPointPanel(opts) {
	var self = this,
		ctx = opts.ctx,
		kadrNum = opts.frames,
		kadr = 0,
		arrow = String.fromCharCode(187),
		trademark = String.fromCharCode(174),
		fs = Math.round(opts.g.h/50),
		font = fs + "px futura",
		bfs = Math.round(fs * 3),
		bfont = bfs + "px futura-bol",
		ltrW = getTextWidth(ctx, font, "LEITERARTEN"),
		initX = Math.round(opts.g.w * 0.45),
		finX = initX,
		initY = (opts.g.h - fs * 7 - bfs * 4)/2,
		fullDiff = opts.g.w - initX,
		panelX = initX - bfs,
		buttonDiff = initX - bfs * 2,
		panel = new Panel({
			x: panelX,
			y: initY,
			h: fs * 5 + bfs * 4.5,
			w: initX,
			color: "#000"
		}),
		elements = new PanelElements(fullDiff, finX, [
			{
				text: "LEITERARTEN",
				x: -fullDiff,
				dir: "lr",
				y: initY + fs,
				font: font,
				color: "#FFF"
			},
			{
				text: "EINE FÜR ALLE.",
				x: -fullDiff,
				dir: "lr",
				y: initY + fs * 4,
				font: bfont,
				color: "#FFF"
			},
			{
				text: "ALLE IN EINE.",
				x: -fullDiff,
				dir: "lr",
				y: initY + fs * 4 + bfs,
				font: bfont,
				color: "#FFF"
			},
			{
				text: "TOPJOB" + trademark + "S Reihenklemmen mit sicherer",
				x: opts.g.w,
				dir: "rl",
				y: initY + fs * 4 + bfs * 2.5,
				font: font,
				color: "#FFF"
			},
			{
				text: "Push-in CAGE CLAMP" + trademark + " - für alle Leiterarten",
				x: opts.g.w,
				dir: "rl",
				y: initY + fs * 5 + bfs * 2.5,
				font: font,
				color: "#FFF"
			},
			{
				x: -fullDiff,
				dir: "lr",
				y: initY + fs * 2 + fs/2,
				w: ltrW,
				h: fs/3,
				color: "#57AA53"
			}
		]),
		button = new Button({
			ctx: ctx,
			x: opts.g.w,
			y: initY + fs * 7 + bfs * 2.5,
			hFactor: 1.5,
			wFactor: 1.3,
			fs: fs,
			font: "futura",
			color: "#57AA53",
			activeColor: "#4E994A",
			text: arrow + " Mehr erfahren",
			textColor: "#FFF"
		}),
		activeButton;

	var detailImage = new Image(),
		imgX, imgY;

	detailImage.onload = function () {
		imgX = opts.g.cx - detailImage.width/2;
		imgY = opts.g.cy - detailImage.height/2;
	};
	detailImage.src = "images/misc/leiterarten.png";



	function PanelElements(fullpath, finX, els) {
		var self = this,
			len = els.length;
		self.draw = function () {
			var i = 0, elem;
	        ctx.save();
	            ctx.textBaseline = "top";
	            for ( ; i < len; i++ ) {
	            	elem = els[i];
	            	ctx.fillStyle = elem.color;
	            	if ( elem.text ) {
	            		ctx.font = elem.font;
	            		ctx.fillText(elem.text, elem.x, elem.y);
	            	}else {
	            		ctx.fillRect(elem.x, elem.y, elem.w, elem.h);
	            	}
	            }
	        ctx.restore();
		};

		self.update = function (lx, rx) {
            for ( var i = 0; i < len; i++ ) {
            	els[i].x = ( els[i].dir === "lr" ) ? lx : rx;
            }
		};
	}

	self.start = opts.start;

	self.init = function () {
		kadr = 0;
	};

	self.listen = function () {
		setEventListeners();
	};

	self.reset = function () {
		removeEventListener();
	};

	self.draw = function () {
		   //      var text = "This is kadr#" + kadr + " of " + opts.text + " panel";
		   //      ctx.save();
		   //          ctx.textAlign = "center";
		   //          ctx.textBaseline = "middle";
					// ctx.font = "24px futura";
					// ctx.fillStyle = "#AAA";
		   //          ctx.fillText(text, cx, cy);
		   //      ctx.restore();
        panel.draw();
        elements.draw();
        button.draw();
	};

	self.update = function (n) {
		var	kadr = n - self.start,
			pr = kadr/kadrNum,
			dx = fullDiff * (1 - pr),
			leftX = finX - dx,
			rightX = finX + dx;
        panel.update(pr);
        elements.update(leftX, rightX);
		button.rect.x = rightX + buttonDiff - button.rect.w;
		button.textX = button.rect.x + button.rect.w/2;
	};

	function Panel(o) {
		var self = this,
			finOpacity = 0.4,
			x = o.x,
			y = o.y,
			opacity = 0;
		self.draw = function () {
	        ctx.save();
				ctx.fillStyle = o.color;
				ctx.globalAlpha = opacity;
	            ctx.fillRect(x, y, o.w, o.h);
	        ctx.restore();
		};
		self.update = function (diff) {
			opacity = diff * finOpacity;
		};
	}

    function setEventListeners() {
        ctx.canvas.addEventListener("mousemove", onMouseMoveCHP, false);
        ctx.canvas.addEventListener("click", onMouseClickCHP, false);
    }

    function removeEventListeners() {
        ctx.canvas.removeEventListener("mousemove", onMouseMoveCHP, false);
        ctx.canvas.removeEventListener("click", onMouseClickCHP, false);
    }

    function onMouseMoveCHP(e) {
        var mouse = getPointerPos(e);
        e.target.style.cursor = "auto";
        activeButton = button.checkButton(mouse);
        if ( activeButton ) { e.target.style.cursor = "pointer"; }
        ctx.clearRect(button.rect.x, button.rect.y, button.rect.w, button.rect.h);
        button.draw();
    }

    function onMouseClickCHP() {
        if ( activeButton ) { showDetails(); }
    }

    function showDetails() {
    	if ( imgX ) {
    		removeEventListeners();
    		ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    		ctx.drawImage(detailImage, imgX, imgY);
    	}
    	opts.onShowDetails();
    }

}
},{}]},{},[1])