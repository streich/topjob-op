(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
/* jslint ignore:start */

function ContactPage(ctx, g, onBtnClick) {
	var self = this,
		fs = Math.round(g.h/18),
		fstj = Math.round(fs * 1.2),
		initX = g.w * 0.16,
		font = fs + 'px futura',
		fontTJ = fstj + 'px futura-bol',
		fontS = fstj + 'px symbol1',
		fontTM = (fs * 0.6) + 'px futura-bol',
		y1 = g.h * 0.16,
		y2 = y1 + fstj * 1.2,
		y3 = y2 + fstj * 1.2,
		trademark = String.fromCharCode(174),
		arrow = String.fromCharCode(8250),
		secondRowX = getXs();
//		button1 = new Button({
//			ctx: ctx,
//			x: initX,
//			y: y3,
//			hFactor: 1.8,
//			wFactor: 1.3,
//			fs: Math.round(fs/2),
//			font: 'futura',
//			color: '#57AA53',
//			activeColor: '#4E994A',
//			text: arrow + ' Kontakt aufnehmen',
//			textColor: '#FFF'
//		}),
//		btnW = button1.rect.w,
//		button2 = new Button({
//			ctx: ctx,
//			x: button1.rect.x + btnW * 1.3,
//			y: y3,
//			w: btnW,
//			h: button1.rect.h,
//			fs: Math.round(fs/2),
//			font: 'futura',
//			color: '#57AA53',
//			activeColor: '#4E994A',
//			text: arrow + ' Muster bestellen',
//			textColor: '#FFF'
//		}),
//		activeButton;

	function getXs() {
		var tj, tm, s;
		ctx.save();
		    ctx.font = fontTM;
		    tm = ctx.measureText(trademark).width;
		    ctx.font = fontTJ;
		    tj = ctx.measureText('TOPJOB').width;
		    ctx.font = fontS;
		    s = ctx.measureText('@').width;
		ctx.restore();
		return {
		    x1: initX,
		    x2: initX + tj + tm/2,
		    x3: initX + tj + tm * 1.5,
		    x4: initX + tj + tm + s,
		};
	}

	self.init = function () {
		setEventListeners();
		self.draw();
	};

	self.removeEventListeners = function () {
		removeEventListeners();
	};

	self.draw = function () {
		ctx.clearRect(0, 0, g.w, g.h);
		ctx.save();
            ctx.fillStyle = '#333';
            ctx.font = font;
            ctx.fillText('Sie wollen', initX, y1);
            ctx.font = fontTJ;
            ctx.fillText('TOPJOB', secondRowX.x1, y2);
            ctx.font = fontTM;
            ctx.fillText(trademark, secondRowX.x2, y2 - fs * 0.3);
            ctx.font = fontS;
            ctx.fillText('@', secondRowX.x3, y2 + fs * 0.05);
            ctx.font = font;
            ctx.fillText(' kennenlernen?', secondRowX.x4, y2);
            button2.draw();
            button1.draw();
        ctx.restore();
	};

    function setEventListeners() {
        ctx.canvas.addEventListener('mousemove', onMouseMoveCF, false);
        ctx.canvas.addEventListener('click', onMouseClickCF, false);
    }

    function removeEventListeners() {
        ctx.canvas.removeEventListener('mousemove', onMouseMoveCF, false);
        ctx.canvas.removeEventListener('click', onMouseClickCF, false);
    }

    function onMouseMoveCF(e) {
        var mouse = getPointerPos(e);
        e.target.style.cursor = 'auto';
        activeButton = button1.checkButton(mouse);
        if ( !activeButton ) { activeButton = button2.checkButton(mouse); }
        if ( activeButton ) { e.target.style.cursor = 'pointer'; }
        self.draw();
    }

    function onMouseClickCF() {
        var text;
        if ( activeButton ) {
        	text = activeButton.getText().replace(arrow + ' ', '');
            onBtnClick(text);
        }
    }

}

},{}]},{},[1])