(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';
/* jslint ignore:start */

function Arch(o) {
	var self = this,
		ctx = o.ctx,
		color = o.color,
		r = o.r,
		cx = o.cx,
		cy = o.cy,
		startAngle = o.startAngle,
		maxAngle = o.maxAngle,
		angle = o.angle,
		finAngle = startAngle + angle,
		ccw = o.ccw,
		lineWidth = o.lineWidth,
		// dir = 1,
		dotA = {
			x: cx + Math.cos(startAngle) * r,
			y: cy + Math.sin(startAngle) * r,
			r: o.dotRadius
		},
		dotB = {
			x: cx + Math.cos(finAngle) * r,
			y: cy + Math.sin(finAngle) * r,
			r: o.dotRadius
		};

	self.update = function(rotation, dAngle) {
		startAngle += rotation;
		if ( dAngle ) {
			// angle += dAngle * dir;
			angle += dAngle;
			if ( Math.abs(angle) > maxAngle ) {
				angle = maxAngle;
				// dir *= -1;
			}
			// if ( Math.abs(angle) < minAngle ) {
			// 	angle = minAngle;
			// 	dir *= -1;
			// }
		}
		finAngle = startAngle + angle;
		dotA = {
			x: cx + Math.cos(startAngle) * r,
			y: cy + Math.sin(startAngle) * r,
			r: o.dotRadius
		};
		dotB = {
			x: cx + Math.cos(finAngle) * r,
			y: cy + Math.sin(finAngle) * r,
			r: o.dotRadius
		};
	};

	self.draw = function() {
		ctx.save();
			ctx.beginPath();
			ctx.arc(cx, cy, r, startAngle, finAngle, ccw);
			ctx.lineWidth = lineWidth;
			ctx.strokeStyle = color;
			ctx.stroke();
			ctx.beginPath();
			ctx.arc(dotA.x, dotA.y, dotA.r, 0, Math.PI * 2, false);
			ctx.fillStyle = color;
			ctx.fill();
			ctx.beginPath();
			ctx.arc(dotB.x, dotB.y, dotB.r, 0, Math.PI * 2, false);
			ctx.fillStyle = color;
			ctx.fill();
		ctx.restore();
	};
}

function Glow(o) {
	var self = this,
		ctx = o.ctx,
		maxr = o.r,
		scaleH = 1,
		color = 51,
		maxcolor = 154,
		r = 10,
		done = false,
		opacity = 1;

	self.draw = function() {
		var grdt;
		if ( opacity ) {
//			ctx.save();
//				ctx.globalAlpha = opacity;
//				ctx.translate(o.cx, o.cy);
//				grdt = ctx.createRadialGradient(0, 0, 5, 0, 0, o.r);
//				grdt.addColorStop(0, getRgba(color, 0.5));
//				grdt.addColorStop(1, getRgba(color, 0));
//				ctx.scale(scaleH, 1);
//				ctx.beginPath();
//				ctx.arc(0, 0, o.r, 0, 2 * Math.PI, false);
//				ctx.fillStyle = grdt;
//				ctx.fill();
//			ctx.restore();
		}
	};

	self.update = function(sst) {
		if ( opacity ) {
			if ( !done ) {
				if ( r !== maxr ) {
					r += 0.5;
					if ( r >= maxr ) { r = maxr; }
				}
				if ( color !== maxcolor ) {
					color += 1;
					if ( color >= maxcolor ) { color = maxcolor; }
				}else {
					scaleH += 0.03;
					if ( scaleH >= 2 ) {
						scaleH = 2;
						done = true;
					}

				}
			}else if ( !sst ) {
				opacity -= 0.01;
				if ( opacity <= 0 ) { opacity = 0; }
			}
		}
	};

	function getRgba(br, op) {
		return 'rgba(' + br + ',' + br + ',' + br + ',' + op + ')';
	}
}

function Pribor(o) {
	var self = this,
		ctx = o.ctx,
		opacity = 0,
		// fs = Math.round(o.r/3.8),
		// fontTJ = fs + 'px futura-dem',
		// fontS = fs + 'px symbol1',
		// fontTM = (Math.round(fs/2)) + 'px futura-dem',
		// trademark = String.fromCharCode(174),
		// textPos = getTextPos(),
		w = o.r,
		x, y, h,
		image, ratio;

        // function getTextPos() {
        // 	var initY = o.y + fs/2,
        // 		initX, tj, tm, s;
        //     ctx.save();
        //         ctx.font = fontTJ;
        //         tj = ctx.measureText('TOPJOB').width;
        //         ctx.font = fontTM;
        //         tm = ctx.measureText(trademark).width;
        //         ctx.font = fontS;
        //         s = ctx.measureText('@').width;
        //     ctx.restore();
        //     initX = o.x - (tj + tm + s)/2;
        //     return {
        //         tj: { x: initX, y: initY },
        //         tm: { x: initX + tj - tm/2, y: initY - fs/2.5 },
        //         s: { x: initX + tj - tm/2 + tm, y: initY }
        //     };
        // }

	self.done = false;

	self.draw = function() {
		if ( opacity ) {
			ctx.save();
				ctx.globalAlpha = opacity;
				// ctx.fillStyle = '#FFF';
				ctx.drawImage(image, x, y, w, h);
				// ctx.font = fontTJ;
				// ctx.fillText('TOPJOB', textPos.tj.x, textPos.tj.y);
				// ctx.font = fontTM;
				// ctx.fillText(trademark, textPos.tm.x, textPos.tm.y);
				// ctx.font = fontS;
				// ctx.fillText('@', textPos.s.x, textPos.s.y);
			ctx.restore();
		}
	};

	self.update = function(sst) {
		if ( !sst && !self.done ) {
			opacity += 0.003;
			// w += 2;
			// h = w * ratio;
			if ( opacity >= 1 ) {
				opacity = 1;
				self.done = true;
			}
			// if ( w > o.r ) { self.done = true; }
		}
	};

	self.setImage = function(img) {
		image = img;
		ratio = img.height/img.width;
		h = w * ratio;
		x = o.x - img.width/2;
		y = o.y - img.height/2;
	};
}

function Slogan(o) {
	var self = this,
		ctx = o.ctx,
		fs1 = Math.round(o.w/11),
		fs2 = Math.round(o.w/8),
		fs2x3 = fs1 * 3,
		fs3 = Math.round(o.w/18),
		text1 = {
			text: [o.text1],
			font: fs1 + 'px futura',
			x: o.x,
			y: o.y - fs2x3,
			color: 51,
			show: true
		},
		text2 = {
			text: [o.text2],
			font: fs2 + 'px futura-bol',
			x: o.x,
			y: o.y,
			color: 51,
			show: false
		},
		text3 = {
			text: [o.text3, o.text4],
			font: fs3 + 'px futura',
			x: o.x,
			y: o.y + fs2x3,
			dy: fs3 * 1.5,
			color: 51,
			show: false
		},
		opacity = 1,
		stage = 'appearance';

	self.stay = 200;
	self.done = false;

	self.draw = function() {
		if ( !self.done ) {
//			ctx.save();
//	            ctx.textAlign = 'center';
//	            ctx.textBaseline = 'middle';
//	            ctx.globalAlpha = opacity;
//	            drawText(text1);
//	            drawText(text2);
//	            drawText(text3);
//			ctx.restore();
		}
	};

	self.update = function() {
		if ( !self.done ) {
			if ( stage === 'appearance' ) {
				if ( text1.color !== 255 ) {
					text1.color += 2;
					if ( text1.color === 255 ) { text2.show  = true; }
				}else if ( text2.color !== 255 ) {
					text2.color += 2;
					if ( text2.color === 255 ) { text3.show  = true; }
				}else {
					text3.color += 2;
					if ( text3.color === 255 ) { stage = 'disappearance'; }
				}
			}else {
				if ( self.stay ) {
					self.stay--;
				}else {
					text1.color -= 2;
					text2.color -= 2;
					text3.color -= 2;
					opacity -= 0.01;
					if ( opacity <= 0 ) {
						opacity = 0;
						self.done = true;
					}
				}
			}
		}
	};

	function drawText(t) {
		if ( !t.show ) { return; }
		ctx.font = t.font;
		ctx.fillStyle = 'rgb(' + t.color + ',' + t.color + ',' + t.color + ')';
		ctx.fillText(t.text[0], t.x, t.y);
		if ( t.text[1] ) { ctx.fillText(t.text[1], t.x, t.y + t.dy); }
	}
}

function InitAnimation(ctx, g, callback) {

	var self = this,
		r1 = Math.round(g.h/4),
		r2 = Math.round(g.h/3.6),
		// dcx = (r2 - r1)/1.5,
		trademark = String.fromCharCode(174),
		slogan = new Slogan({
			ctx: ctx,
			w: r2,
			x: g.cx,
			y: g.cy,
			text1: 'Wir haben keine neue push-in Technologie',
			text2: 'WIR HABEN SIE ERFUNDEN',
			text3: 'Und zwar 1951. Setzen Sie auch heute auf unsere grosse Erfahrung',
			text4: '.TOPJOB' + trademark + 'S mit sicherer Push-in CAGE CLAMP' + trademark + '.'
		}),
		glow = new Glow({
			ctx: ctx,
			r: Math.round(r2/2.5),
			cx: g.cx,
			cy: g.cy
		}),
		pribor = new Pribor({
			ctx: ctx,
			r: r2 * 2.2,
			x: g.cx,
			y: g.cy
		}),
		// archGreen = new Arch({
		// 	ctx: ctx,
		// 	color: '#549452',
		// 	r: r1,
		// 	cx: g.cx,
		// 	// cx: g.cx - dcx,
		// 	cy: g.cy,
		// 	startAngle: 0,
		// 	angle: 0,
		// 	maxAngle: Math.PI/1.5,
		// 	minAngle: Math.PI/4,
		// 	ccw: false,
		// 	lineWidth: 2,
		// 	dotRadius: 4
		// }),
		// archBlue = new Arch({
		// 	ctx: ctx,
		// 	color: '#057AC0',
		// 	r: r2,
		// 	cx: g.cx,
		// 	cy: g.cy,
		// 	startAngle: Math.PI,
		// 	angle: 0,
		// 	maxAngle: Math.PI/1.5,
		// 	minAngle: Math.PI/4,
		// 	ccw: false,
		// 	lineWidth: 2,
		// 	dotRadius: 4
		// }),

		// speed = {
		// 	rotation: Math.PI/35,
		// 	growth: Math.PI/50,
		// },
		// tick = 0,
		rAF;

	self.running = false;

	self.run = function () {
		self.running = true;
		rAF = requestAnimationFrame(loop);
	};

	self.stop = function (cb) {
		cancelAnimation();
		if ( cb && typeof cb === 'function' ) { cb(); }
	};

	self.setImage = function (img) {
		pribor.setImage(img);
	};

	function loop() {
		loop.tick = loop.tick || 0;
		clear();
		update();
		draw();
		if ( rAF ) {
			loop.tick++;
			// if ( loop.tick === 80 ) {
			if ( pribor.done && callback ) {
				callback();
				callback = null;
			}
			rAF = requestAnimationFrame(loop);
		}
	}

    function clear() {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
    }

	function draw() {
		// archGreen.draw();
		// archBlue.draw();
//		glow.draw();
//		slogan.draw();
//		pribor.draw();
	}

	function update() {
		// archGreen.update(speed.rotation, speed.growth);
		// archBlue.update(-speed.rotation, speed.growth);
		slogan.update();
		glow.update(slogan.stay);
		pribor.update(slogan.stay);
	}

	function cancelAnimation() {
		self.running = false;
		cancelAnimationFrame(rAF);
		rAF = 0;
	}
}

},{}]},{},[1])