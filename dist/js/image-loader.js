(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);throw new Error("Cannot find module '"+o+"'")}var f=n[o]={exports:{}};t[o][0].call(f.exports,function(e){var n=t[o][1][e];return s(n?n:e)},f,f.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
/* global console */

function loadImages(ctx, callback1, callback2) {

    var i = 75, // 86
        finIndex = 634,  // 634
        firstCheckPoint = 190,
        frameToRunAnimation = firstCheckPoint - i,
        sources = [],
        frames = [],
        directory = "client/images/frames/",
        templateUrl = "frame_0",
        progress = new LoadProgress(ctx),
        url, num, iId,
        uploaded = 0;

    for ( ; i < finIndex; i++ ) {
        url = directory + templateUrl + ( ( i < 100 ) ? (( i < 10 ) ? "00" : "0") : "" ) + i + ".jpg";
        sources.push(url);
    }

    num = sources.length;

    iId = setInterval(function () {
        var x = 1 - ((frameToRunAnimation - uploaded)/frameToRunAnimation);
        progress.draw(x);
    }, 200);

    for ( i = 0; i < sources.length; i++ ) {
        frames[i] = new Image();
        frames[i].onload = function() {
            num--;
            uploaded++;
            if ( uploaded === frameToRunAnimation ) {
                callback1();
                clearInterval(iId);
            }else if ( !num ) {
                // console.log("all images loaded!");
                callback2(frames);
            }
        };

        frames[i].src = sources[i];
    }

    function LoadProgress(ctx) {
        var self = this,
            r = Math.round(ctx.canvas.height/24),
            fs = Math.round(r/1.2),
            cx = ctx.canvas.width/2,
            cy = ctx.canvas.height/2,
            color = "#AAA",
            font = fs + "px futura",
            startAngle = Math.PI * 1.5;
        self.draw = function (x) {
            var finAngle = startAngle + x * Math.PI * 2,
                pr = x * 100,
                text = pr.toFixed(0) + "%";
            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            ctx.save();
                ctx.textAlign = "center";
                ctx.textBaseline = "middle";
                ctx.lineWidth = "3";
                ctx.strokeStyle = color;
                ctx.fillStyle = color;
                ctx.font = font;
                ctx.beginPath();
                ctx.arc(cx, cy, r, startAngle, finAngle, false);
                ctx.stroke();
                ctx.fillText(text, cx, cy);
            ctx.restore();
        };
    }
}

},{}]},{},[1])