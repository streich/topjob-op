'use strict';
/* jslint browser:true */

/**
 * public string getQueryValue([string] name, [string] url)
 *
 * Returns the value of the query parameter from a url
 */
module.exports = function getQueryValue(name, url) {
    var query, vars;

    url = url || window.location.href;
    query = url.substring(url.indexOf('?') + 1);

    vars = query.split('&');

    // Search query values
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (pair[0] == name) return pair[1];
    }

    // Return empty string if value isn't given
    return '';
};

