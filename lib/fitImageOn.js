'use strict';
var debug = require('debug')('wago:lib:fitOnCanvas');

/**
 * Fit an image on a canvas
 *
 * code from:
 * http://sdqali.in/blog/2013/10/03/fitting-an-image-in-to-a-canvas-object/
 */
module.exports = function fitImageOnCanvas(canvas, context, imageObj) {
    var imageAspectRatio = imageObj.width / imageObj.height;
    var canvasAspectRatio = canvas.width / canvas.height;
    var renderableHeight, renderableWidth, xStart, yStart;

    // If image's aspect ratio is less than canvas's we fit on height
    // and place the image centrally along width
    if(imageAspectRatio > canvasAspectRatio) {
        renderableHeight = canvas.height;
        renderableWidth = imageObj.width * (renderableHeight / imageObj.height);
        xStart = (canvas.width - renderableWidth) / 2;
        yStart = 0;
    }

    // If image's aspect ratio is greater than canvas's we fit on width
    // and place the image centrally along height
    else if(imageAspectRatio < canvasAspectRatio) {
        renderableWidth = canvas.width;
        renderableHeight = imageObj.height * (renderableWidth / imageObj.width);
        xStart = 0;
        yStart = (canvas.height - renderableHeight) / 2;
    }

    // Happy path - keep aspect ratio
    else {
        renderableHeight = canvas.height;
        renderableWidth = canvas.width;
        xStart = 0;
        yStart = 0;
    }

    debug('fitOnCanvas: ', {
        xStart: xStart,
        yStart: yStart,
        renderableWidth: renderableWidth,
        renderableHeight: renderableHeight,
    });

    context.drawImage(imageObj, xStart, yStart, renderableWidth, renderableHeight);
};
