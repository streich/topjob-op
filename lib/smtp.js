'use strict';

var debug = require('debug')('lftsqt:smtp');

var config = require('../config'),
    mandrill = require('mandrill-api/mandrill');


/**
 * Client for handling smtp calls
 */
function Smtp(message, options) {
    this.message = typeof(message) === 'object' ?
        message :
        config.mandrill.defaultMessage || {};

    // Setup mandrill client
    var mandrillClient = new mandrill.Mandrill(config.mandrill.key);
    this.client = mandrillClient;

    // Setup options, and defaults
    this.options = typeof(options) === 'object' ? options : {};

    if (typeof(this.options.templateName) !== 'object')
        this.options.templateName = config.mandrill.defaultTemplate || 'lftsqtweb_default';
}

Smtp.prototype.sendTemplate =
function(cb, errCb, message) {
    return this.client.messages.sendTemplate({
        template_name: this.options.templateName,
        template_content: this.options.templateContent,
        message: this.getMessage(message),
        async: this.options.async,
        ip_pool: this.options.ipPool,
        send_at: this.options.sendAt
    }, function(response) {
        cb(null, response);
    }, function(err) {
        cb(err);
    });
};

Smtp.prototype.send =
function(cb, message) {
    return this.client.messages.send({
        message: this.getMessage(message),
        async: this.options.async,
        ip_pool: this.options.ipPool,
        send_at: this.options.sendAt
    }, function(response) {
        debug('sent message: ', message, response);
        cb(null, response);
    }, function(err) {
        debug('error sending message: ', message, err);
        cb(err);
    });
};

Smtp.prototype.getMessage =
function(message) {
    message = typeof(message) === 'object' ?
        message :
        this.message || config.mandrill.defaultMessage || {};

    /*
     * An array of default values
     * name: name of the message key
     * value: a default value to be used as last resort
     */
    var defaults = [
        { name: 'subject'                   },
        { name: 'from_email'                },
        { name: 'from_name'                 },
        { name: 'auto_text',    value: true },
        { name: 'inline_css',   value: true },
    ];

    for (var i = 0; i < defaults.length; i++) {
        var def = defaults[i];

        message[def.name] =
            message[def.name] || this.message[def.name] ||
            (config.mandrill.message && config.mandrill.message[def.name]) ||
            config.mandrill[def.name] ||
            def.value;
    }

    return message;
};

Smtp.prototype.setMessage =
function(message) {
    this.message = this.getMessage(message);

    return this;
};

module.exports = exports = Smtp;
